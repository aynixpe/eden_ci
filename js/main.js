function calcular_dias(inicio, fin){
    var aFecha1 = inicio.split('-');
    var aFecha2 = fin.split('-');
    var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]);
    var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]);
    var dif = fFecha2 - fFecha1;
    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
    return dias;
}

function procedencia(origen, destino){
    $(origen).typeahead({
        ajax: './clientes/get_ubigeo',
        onSelect: function(item){
            $(destino).val(item.value);
        }
    });
}

function timepicker(elemento){
    $(elemento).timepicker({
        minuteStep: 1,
        secondStep: 1,
        showInputs: false,
        disableFocus: true,
        showSeconds: true,
        template: 'dropdown'
    });
}

function datepicker(elemento){
    $(elemento).datepicker({
        format: 'yyyy-mm-dd'
    })
    .datepicker('setValue', new Date())
    .data('datepicker');
}

function get_tipos(e){
    $.ajax({
        url: base_url + "usuarios/get_tipos",
        type: 'post',
        dataType: 'json',
        success: function(result){
            e(result);
        }
    });
}

function get_tipos_comprobante(e){
    $.ajax({
        url: base_url + "comprobantes/get_tipos",
        type: 'post',
        dataType: 'json',
        success: function(result){
            e(result);
        }
    });
}

function get_series(idTipoComprobante, e){
    $.ajax({
        url: base_url + "comprobantes/get_series",
        type: 'post',
        dataType: 'json',
        data: {
            idTipoComprobante: idTipoComprobante
        },
        success: function(result){
            e(result);
        }
    });
}

function get_usuarios(e){
    $.ajax({
        url: base_url + "usuarios/get_usuarios",
        type: 'post',
        dataType: 'json',
        success: function(result){
            e(result);
        }
    });
}

function cargar_usuario(idUsuario, e){
    $.ajax({
        url: base_url + 'usuarios/get_usuario',
        type: 'post',
        dataType: 'json',
        data: {
            idUsuario : idUsuario
        },
        success: function(result){
            e(result);
        }
    });
}

function limpiarRegistroCliente(){
    $('#cboTipoDocumento option[value="1"]').attr('selected', true); // value 1
    $('#txtNumDoc').val('');
    $('#txtRUC').val('');
    $('#txtNombres').val('');
    $('#txtApellidos').val('');
    $('#txtProcedencia').val('');
    $('#idProcedenciaPersona').val('-1');
    $('#txtTelefono').val('');
    $('#txtCorreo').val('');
    $('#alertRegistroPersona').html('');
    $('#txtRUC-empresa').val('');
    $('#txtRazonSocial').val('');
    $('#txtProcedencia-empresa').val('');
    $('#idProcedenciaEmpresa').val('-1');
    $('#txtDireccion').val('');
    $('#txtTelefono-empresa').val('');
    $('#txtCorreo-empresa').val('');
    $('#alertRegistroEmpresa').html('');
}

function actualizarTotal(){
    var total = 0;
    var itemsVenta = $('#resultadoBusquedaCuentas').children();
    for (var i = itemsVenta.length - 1; i >= 0; i--) {
        total += parseFloat(itemsVenta.get(i).getAttribute('monto'));
    };
    $('#total').text("Total: S/." + total);
}

function actualizarSeleccion(){
    var total = 0;
    var itemsVenta = $('#resultadoBusquedaCuentas').children();
    for (var i = itemsVenta.length - 1; i >= 0; i--) {
        var tds = $(itemsVenta.get(i)).children();
        var chb = $(tds.get(0)).children().get(0);
        if( $(chb).is(':checked') ){
            total += parseFloat(itemsVenta.get(i).getAttribute('monto'));
        }
    };
    $('#totalSelect').text("Seleccionado: S/." + total);
}

function comprobar_permiso(valor, accion){
    return ((valor & accion) == accion);
}
$('#btnPagar').on('click', function(e){
    var j = 1;
    var cadenaHtml = "";
    var items = $('#resultadoBusquedaCuentas').children();
    for (var i = 0; i < items.length; i++) {
        var tds = $(items.get(i)).children();
        var chb = $(tds.get(0)).children().get(0);
        if( $(chb).is(':checked') ){
            var fila = items.get(i);
            cadenaHtml += "<tr class='fila' idCuenta='" + fila.getAttribute('idCuenta') + "' ";
            cadenaHtml += "monto='" + fila.getAttribute('monto') + "' descripcion='" + fila.getAttribute('descripcion') + "' ";
            cadenaHtml += "cantidad='" + fila.getAttribute('cantidad') + "'><td>";
            cadenaHtml += j + "</td><td>";
            cadenaHtml += fila.getAttribute('descripcion');
            cadenaHtml += "</td><td>" + fila.getAttribute('cantidad');
            cadenaHtml += "</td><td>S/." + fila.getAttribute('monto') + "</td></tr>";
            j++;
        }
    };
    $('#tblItemsComprobante').html(cadenaHtml);
    $('#mdlRegistroComprobante').modal('show');
});
$('#btnBuscarCliente').on('click', function(e){
	$('#tipoBusqueda').val('1');
	$('#mdlBuscarCliente').modal('show');
});
$('#btnBuscarHuesped').on('click', function(e){
    $('#tipoBusqueda').val('2');
    $('#mdlBuscarCliente').modal('show');
});
$("#frmRegistroPersona").on('submit', function(e){
    e.preventDefault();

    tipoDocumento = $('#cboTipoDocumento').val();
    documento = $('#txtNumDoc').val();
    ruc = $('#txtRUC').val();
    nombres = $('#txtNombres').val();
    apellidos = $('#txtApellidos').val();
    ubigeo = $('#idProcedenciaPersona').val();
    telefono = $('#txtTelefono').val();
    correo = $('#txtCorreo').val();
    if(ubigeo == -1){
        var x = '<div class="alert alert-warning alert-dismissible" role="alert">';
        x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
        x += '<span class="sr-only">Cerrar</span></button>Procedencia no seleccionada</div>';

        $("#alertRegistroPersona").html(x);
    }else{
        $.ajax({
            url: base_url + 'clientes/insertar_cliente',
            type:'POST',
            dataType: 'json',
            data:{
                tipo : 'P',
                tipoDocumento : tipoDocumento,
                documento : documento,
                ruc : ruc,
                nombres : nombres,
                apellidos : apellidos,
                ubigeo : ubigeo,
                telefono : telefono,
                correo : correo
            },
            success: function(result){
                var x = '<div class="alert alert-success alert-dismissible" role="alert">';
                x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
                x += '<span class="sr-only">Cerrar</span></button>Registrado correctamente</div>';
                $("#alertRegistroPersona").html(x);
            }
        });
    }
});
$("#frmRegistroEmpresa").on('submit', function(e){
    e.preventDefault();

    ruc = $('#txtRUC-empresa').val();
    razonsocial_empresa = $('#txtRazonSocial').val();
    ubigeo = $('#idProcedenciaEmpresa').val();
    direccion = $('#txtDireccion').val();
    telefono = $('#txtTelefono-empresa').val();
    correo = $('#txtCorreo-empresa').val();

    if(ubigeo == -1){
        var x = '<div class="alert alert-warning alert-dismissible" role="alert">';
        x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
        x += '<span class="sr-only">Cerrar</span></button>Procedencia no seleccionada</div>';

        $("#alertRegistroEmpresa").html(x);
    }else{
        $.ajax({
            url: base_url + 'clientes/insertar_cliente',
            dataType: 'json',
            type:'POST',
            data:{
                tipo: 'E',
                ruc : ruc,
                razon_social : razonsocial_empresa,
                ubigeo : ubigeo,
                direccion : direccion,
                telefono : telefono,
                correo : correo
            },
            success: function(result){
                var x = '<div class="alert alert-success alert-dismissible" role="alert">';
                x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
                x += '<span class="sr-only">Cerrar</span></button>Registrado correctamente</div>';
                $("#alertRegistroEmpresa").html(x);
            }
        });
    }
});
