<?php
class Cuentas extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('cuentas_model', 'usuarios_model'));
        $this->load->library('session');
    }
    public function info_cuenta(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $idHabitacion = $this->input->post('idHabitacion');
            $data['data'] = $this->cuentas_model->getCuenta($idHabitacion);
        }
        echo json_encode($data);
    }

    public function get_cuentas(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $query = $this->input->post('query');
            $data['data'] = $this->cuentas_model->getCuentas_query();
        }
        echo json_encode($data);
    }

    public function info_cuenta_cliente(){
    	if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $idCliente = $this->input->post('idCliente');
            $data['data'] = $this->cuentas_model->getCuenta_byCliente($idCliente);
        }
    	echo json_encode($data);
    }

    public function detalle_cuenta(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $idCuenta = $this->input->post('idCuenta');
            $data['data'] = $this->cuentas_model->getDetalleCuenta($idCuenta);
        }
        echo json_encode($data);
    }

    public function detalles_cuenta(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $idCuenta = $this->input->post('idCuenta');
            $data['data'] = $this->cuentas_model->getDetallesCuenta($idCuenta);
        }
        echo json_encode($data);
    }

    public function cuentas_cliente(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CUENTAS,
            CONSULTAR
        );
        if($data['state']){
            $idCliente = $this->input->post('idCliente');
            $data['data'] = $this->cuentas_model->getCuentas_byCliente($idCliente);
        }
        echo json_encode($data);
    }
}
?>
