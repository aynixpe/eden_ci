<?php
class Comprobantes extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model(array('comprobantes_model', 'usuarios_model'));
	}

	public function get_tipos(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			COMPROBANTES,
			CONSULTAR
		);
		if($data['state']){
			$data['data'] = $this->comprobantes_model->get_tipos();
		}
		echo json_encode($data);
	}

	public function get_series(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			COMPROBANTES,
			CONSULTAR
		);
		if($data['state']){
			$idTipoComprobante = $this->input->post('idTipoComprobante');
			$data['data'] = $this->comprobantes_model->get_series($idTipoComprobante);
		}
		echo json_encode($data);
	}

	public function insert(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			COMPROBANTES,
			AGREGAR
		);
		if($data['state']){
			$idUsuario = $this->session->userdata('idUsuario');
			$idTipoComprobante = $this->input->post('idTipoComprobante');
			$idCliente = $this->input->post('idCliente');
			$fecha = $this->input->post('fecha');
			$monto = $this->input->post('monto');
			$detalles = $this->input->post('detalles');

			$data['state'] = $this->comprobantes_model->insert(
				$idTipoComprobante,
				$idCliente,
				$fecha,
				$monto,
				$idUsuario,
				$detalles);
			if(!$data['state'])
				$data['error'] = unserialize(ERR_CONEXION);
		}

		echo json_encode($data);
	}
}
?>
