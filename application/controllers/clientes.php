<?php
class Clientes extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('clientes_model','usuarios_model'));
        $this->load->library('session');
    }
	public function gestion(){
    if(!$this->session->userdata('login'))
        redirect('usuarios/ingreso');
    $data = array(
        'usuario' => $this->session->userdata('usuario')
    );
    $this->load->view('header', $data);
		$this->load->view('clientes', $data);
	}

	public function insertar_cliente(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');

    $data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			CLIENTES,
			AGREGAR
		);
    if($data['state']){
        $idUsuario = $this->session->userdata('idUsuario');

        $tipo = $this->input->post('tipo');
        $tipoDocumento = $this->input->post('tipoDocumento');
        $documento = $this->input->post('documento');
        $ruc = $this->input->post('ruc');
        $nombres = $this->input->post('nombres');
        $apellidos = $this->input->post('apellidos');
        $ubigeo = $this->input->post('ubigeo');
        $telefono = $this->input->post('telefono');
        $correo = $this->input->post('correo');
        $razonSocial = $this->input->post('razonSocial');
        $direccion = $this->input->post('direccion');
        $data['state'] = $this->clientes_model->insertarCliente($tipo, $tipoDocumento,
    			$documento, $ruc, $nombres, $apellidos, $ubigeo,
    			$telefono, $correo, $idUsuario, $razonSocial,
    			$direccion);
        if(!$data['state']){
            $data['error'] = unserialize(ERR_CONEXION);
        }
    }
		echo json_encode($data);
	}

	public function buscar_clientes_activos(){
		if(!$this->session->userdata('login'))
      redirect('usuarios/ingreso');
    $data = $this->usuarios_model->comprobar_permiso(
        $this->session->userdata('idUsuario'),
        CLIENTES,
        CONSULTAR
    );
    if($data['state']){
        $query = $this->input->post('query');
        $data['data'] = $this->clientes_model->buscarClientes_activos($query);
    }
		echo json_encode($data);
	}

    public function buscar_empresas_activas(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CLIENTES,
            CONSULTAR
        );
        if($data['state']){
            $query = $this->input->post('query');
            $data['data'] = $this->clientes_model->buscarEmpresas_activas($query);
        }
		echo json_encode($data);
	}

	public function buscar_personas_activas(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CLIENTES,
            CONSULTAR
        );
        if($data['state']){
            $query = $this->input->post('query');
            $data['data'] = $this->clientes_model->buscarPersonas_activas($query);
        }
		echo json_encode($data);
	}

    public function buscar_personas(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            CLIENTES,
            CONSULTAR
        );
        if($data['state']){
            $query = $this->input->post('query');
            $inactivo = $this->input->post('inactivo');
            $noHospedado = $this->input->post('noHospedado');
            $data['data'] = $this->clientes_model->buscarPersonas($query, $inactivo, $noHospedado);
        }
		echo json_encode($data);
    }

    public function get_ubigeo(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $query = $this->input->get('query');
        echo json_encode($this->clientes_model->getUbigeo($query));
    }
}
?>
