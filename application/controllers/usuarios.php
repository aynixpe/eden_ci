<?php
class Usuarios extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('usuarios_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
	}

  	public function gestion(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');

		$data = array(
			'usuario' => $this->session->userdata('usuario')
		);
		$this->load->view('header', $data);
		$this->load->view('usuarios', $data);
  	}

	public function ingreso(){
    	$this->load->view('head');
		$this->load->view('inicio_sesion');
	}

	public function login(){
		$usuario = $this->input->post('usuario');
		$password = md5($this->input->post('password'));

		$chk_user = $this->usuarios_model->login($usuario, $password);

		if($chk_user == TRUE){
			$data = array(
				'login' => TRUE,
				'idUsuario' => $chk_user->idUsuario,
				'usuario' => $chk_user->usuario,
				'tipoUsuario' => $chk_user->idTipoUsuario
			);
			$this->session->set_userdata($data);
			echo json_encode(true);
		}else
			echo json_encode(false);
	}

	public function logout_ci()		{
		$this->session->sess_destroy();
		$this->ingreso();
	}

	public function get_usuarios(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			CONSULTAR
		);
		if($data['state'])
			$data['data'] = $this->usuarios_model->get_usuarios();
		echo json_encode($data);
	}

	public function get_usuario(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			CONSULTAR
		);
		if($data['state']){
			$idUsuario = $this->input->post('idUsuario');
			$data['data'] = $this->usuarios_model->get_usuario($idUsuario);
		}
		echo json_encode($data);
	}

	public function get_tipos(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			CONSULTAR
		);
		if( $data['state'] )
			$data['data'] = $this->usuarios_model->get_tipos();

		echo json_encode($data);
	}

	public function insert_tipo(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			AGREGAR
		);
		if($data['state']){
			$descripcion = $this->input->post('descripcion');
			$data['state'] = $this->usuarios_model->insert_tipo($descripcion);
			if(!$data['state'])
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}

	public function get_tipo(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			CONSULTAR
		);
		if($data['state']){
			$idTipo = $this->input->post('idTipo');
			$data['data'] = $this->usuarios_model->get_tipo($idTipo);
		}
		echo json_encode($data);
	}

	public function update_tipo(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			EDITAR
		);
		if($data['state']){
			$idTipo = $this->input->post('idTipo');
			$descripcion = $this->input->post('descripcion');
			$data['state'] = $this->usuarios_model->update_tipo($idTipo, $descripcion);
			if( !$data['state'] )
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}

	public function editar_permisos(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			EDITAR
		);
		if($data['state']){
			$idTipo = $this->input->post('idTipo');
			$permisos = $this->input->post('permisos');
			$data['state'] = $this->usuarios_model->update_permisos($idTipo, $permisos);
			if(!$data['state'])
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}

	public function borrar_tipo(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			ELIMINAR
		);
		if($data['state']){
			$idTipo = $this->input->post('idTipo');
			$data['state'] = $this->usuarios_model->delete_tipo($idTipo);
			if( !$data['state'] )
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}

	public function insert(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			AGREGAR
		);
		if($data['state']){
			$nombre = $this->input->post('nombre');
			$usuario = $this->input->post('usuario');
			$password = md5($this->input->post('password'));
			$idTipo = $this->input->post('idTipo');
			$data['state'] = $this->usuarios_model->insert($nombre, $usuario, $password, $idTipo);
			if( !$data['state'] )
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}

	public function update(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = $this->usuarios_model->comprobar_permiso(
			$this->session->userdata('idUsuario'),
			USUARIOS,
			EDITAR
		);
		if($data['state']){
			$idUsuario = $this->input->post('idUsuario');
			$nombre = $this->input->post('nombre');
			$usuario = $this->input->post('usuario');
			$idTipo = $this->input->post('idTipo');
			$data['state'] = $this->usuarios_model->update($idUsuario, $nombre, $usuario, $idTipo);
			if( !$data['state'])
				$data['error'] = unserialize(ERR_CONEXION);
		}
		echo json_encode($data);
	}
}
?>
