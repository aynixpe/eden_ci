<?php
class Habitaciones extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('habitaciones_model', 'usuarios_model'));
        $this->load->library('session');
    }

	public function gestion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = array(
            'usuario' => $this->session->userdata('usuario')
        );
        $this->load->view('header', $data);
		$this->load->view('habitaciones', $data);
	}

	public function obtener_tipos_habitacion(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            CONSULTAR
        );
        if($data['state']){
            $data['data'] = $this->habitaciones_model->getTiposHabitacion();
        }
		echo json_encode($data);
	}

    public function agregar_tipo_habitacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            AGREGAR
        );
        if($data['state']){
            $descripcion = $this->input->post('descripcion');
            $precio = $this->input->post('precio');
            $data['state'] = $this->habitaciones_model->addTipoHabitacion($descripcion, $precio);
            if($data['state'])
                $data['data'] = $this->habitaciones_model->getLastTipoHabitacion();
            else
                $data['error'] = unserialize(ERR_CONEXION);
        }
        echo json_encode($data);
    }

    public function agregar_habitacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            AGREGAR
        );
        if($data['state']){
            $data['state'] = $this->habitaciones_model->addHabitacion();
            if(!$data['state'])
                $data['error'] = unserialize(ERR_CONEXION);
        }
        echo json_encode($data);
    }

    public function actualizar_tipo_habitacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            EDITAR
        );
        if($data['state']){
            $data['state'] = $this->habitaciones_model->updateTipoHabitacion();
            if(!$data['state'])
                $data['error'] = unserialize(ERR_CONEXION);
        }
        echo json_encode($data);
    }

    public function actualizar_habitacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            EDITAR
        );
        if($data['state']){
            $data['state'] = $this->habitaciones_model->updateHabitacion();
            if(!$data['state'])
                $data['error'] = unserialize(ERR_CONEXION);
        }
        echo json_encode($data);
    }

	public function obtener_habitaciones(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            SERVICIOS,
            CONSULTAR
        );
        if($data['state']){
            $data['data'] = $this->habitaciones_model->getHabitaciones($this->input->post('estado'));
        }
		echo json_encode();
	}

	public function obtener_habitaciones_tipo(){
		if(!$this->session->userdata('login'))
      redirect('usuarios/ingreso');
    $data = $this->usuarios_model->comprobar_permiso(
      $this->session->userdata('idUsuario'),
      SERVICIOS,
      CONSULTAR
    );
    if($data['state']){
      $idTipo = $this->input->post('idTipo');
      $estado = $this->input->post('estado');
      $data['data'] = $this->habitaciones_model->getHabitaciones_tipo($idTipo, $estado);
    }
		echo json_encode($data);
	}

    public function eliminar_tipo_habitacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        echo json_encode($this->habitaciones_model->eliminarTipoHabitacion());
    }
}
?>
