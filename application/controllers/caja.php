<?php
class Caja extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model(array('caja_model', 'usuarios_model'));
		$this->load->helper('url');
		$this->load->library('session');
	}

	public function gestion(){
		if(!$this->session->userdata('login'))
			redirect('usuarios/ingreso');
		$data = array(
			'usuario' => $this->session->userdata('usuario')
		);
		$this->load->view('header', $data);
		$this->load->view('caja', $data);
	}
}
?>
