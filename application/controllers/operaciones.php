<?php
class Operaciones extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('usuarios_model', 'operaciones_model', 'cuentas_model'));
        $this->load->library('session');
    }

	public function index(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = array(
            'usuario' => $this->session->userdata('usuario')
        );
        $this->load->view('header', $data);
		$this->load->view('operaciones', $data);
	}

	/*public function insertar_reserva(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
		echo json_encode($this->operaciones_model->insertarOperacion('R'));
	}*/

	public function insertar_venta(){
		if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            OPERACIONES,
            AGREGAR
        );
        if($data['state']){
            $idUsuario = $this->session->userdata('idUsuario');

            $idCliente = $this->input->post('idCliente');
            $monto = $this->input->post('monto');
            $observacion = $this->input->post('observacion');
            $fecha = $this->input->post('fecha');
            $hora = $this->input->post('hora');
            $detalles = $this->input->post('detalles');

            $data['state'] = $this->operaciones_model->insertarOperacion(
                $idCliente, $monto, $idUsuario, $observacion, $fecha, $hora, $detalles
            );
            if(!$data['state'])
                $data['error'] = unserialize(ERR_CONEXION);
        }
		echo json_encode($data);
	}

    public function get_detalle_operacion(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            OPERACIONES,
            CONSULTAR
        );
        if($data['state']){
            $idHabitacion = $this->input->post('idHabitacion');
            $estado = 'G';
            $data['data'] = $this->operaciones_model->get_detalle_operacion($idHabitacion, $estado);
        }
        echo json_encode($data);
    }

    public function actualizar_detalle(){
        if(!$this->session->userdata('login'))
            redirect('usuarios/ingreso');
        $data = $this->usuarios_model->comprobar_permiso(
            $this->session->userdata('idUsuario'),
            OPERACIONES,
            EDITAR
        );
        if($data['state']){
            $idCuenta = $this->input->post('idCuenta');
            $idOperacion = $this->input->post('idOperacion');
            $idDetalle = $this->input->post('idDetalleOperacion');
            $cantidad = $this->input->post('cantidad');
            $precio = $this->input->post('precio');

            $data['state'] = $this->operaciones_model->actualizar_detalle($idDetalle, $cantidad, $precio);
            if($data['state']) $data['state'] = $this->cuentas_model->recalcular($idCuenta);
            if(!$data['state']){
                $data['error'] = unserialize(ERR_CONEXION);
            }
        }
        echo json_encode($data);
    }
}
?>
