<!DOCTYPE html>
<html lang='es'>
	<head>
		<meta charset='utf-8'/>
		<title>Hospedaje El Edén</title>
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/non-responsive.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/datepicker.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap-timepicker.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
	</head>
	<body>
		<section class='container'>
			<div class='row'>
				<div class='col-xs-12'>
					<div class='cuadro'>
						<h2 class='text-center'>Empresas</h2>
						<div class='row'>
							<div class='col-xs-7'>
								<form id='frm-buscar' class='row'>
									<div class='col-xs-6'>
										<div class="input-group input-group-sm">
											<input id='txtBusquedaCliente' type='text' class='form-control' placeholder='Buscar cliente...' required />
											<span class='input-group-btn'>
												<button class='btn btn-primary' id='jsBuscarClientes' type='submit'><i class='fa fa-search'></i></button>
											</span>
										</div>
									</div>
									<div class="checkbox col-xs-4 no-margin-top">
										<label class='col-xs-5'>
											<input type="checkbox" id='chbInactivo'>
											Inactivo
										</label>
									</div>
									<div class='col-xs-2'>
										<button type='button' class='btn btn-primary btn-sm pull-right' data-toggle='modal' data-target='#mdlRegistroCliente'>
											<i class='fa fa-plus'></i> Nuevo
										</button>
									</div>
								</form>
								<div class='row'>
									<div class='col-xs-12'>
										<table class='table table-condensed' id='tblBusquedaCliente'>
											<thead>
												<tr>
													<!--th><i class='fa fa-circle estado'></i></th-->
													<th>Razón Social</th>
													<th>RUC</th>
													<th>Teléfono</th>
													<th>Correo</th>
													<th><i class='fa fa-cogs'></i></th>
												</tr>
											</thead>
											<tbody id='resultadoBusquedaEmpresa'>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class='col-xs-5'>
								<h3 class='no-margin-top'>Cuentas</h3>
								<br>
								<table class='table table-condensed' id='tblBusquedaCuentas'>
									<thead>
										<tr>
											<th><i class='fa fa-check-square-o'></i></th>
											<th>Servicio</th>
											<th>Cantidad</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody id='resultadoBusquedaCuentas'>

									</tbody>
								</table>
								<div class='row'>
									<div class="col-xs-6">
										<div class='row'>
											<div class='col-xs-12'>
												<b id='total'>Total: S/.0.00</b>
											</div>
										</div>
										<div class="row">
											<div class='col-xs-12'>
												<b id='totalSelect'>Seleccionado: S/.0.00</b>
											</div>
										</div>
									</div>
									<div class='col-xs-3'>
										<button id='btnPrefactura' type='button' class='btn btn-primary btn-sm col-xs-12'>Prefactura</button>
									</div>
									<div class='col-xs-3'>
										<button id='btnPagar' type='button' class='btn btn-success btn-sm col-xs-12'>Pagar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- MODALES-->
		<div id='mdlRegistroCliente' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroClienteLabel' aria-hidden='true'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="RegistroClienteLabel">Registro de cliente</h4>
					</div>
					<div class='modal-body'>
						<?php $this->load->view('frmRegistroEmpresa'); ?>
					</div>
				</div>
			</div>
		</div>
		<div id='mdlRegistroComprobante' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroComprobanteLabel' aria-hidden='true'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="RegistroComprobanteLabel">Registro de comprobante</h4>
					</div>
					<div class='modal-body'>
						<?php $this->load->view('frmRegistroComprobante'); ?>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-timepicker.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
		<script type='text/javascript'>
			var base_url = '<?php echo base_url(); ?>';
			$(document).ready(function(e){
				$('#mnu-empresas').addClass('active');
				timepicker($('#txtHora'));

				$('#frm-buscar').on('submit', function(e){
					e.preventDefault();
					cargarClientes();
				});
				$('#tblBusquedaCliente tbody').on('click', 'tr', function(e){
					//var idCliente =
					cargarCuentas( $(this).attr('idCliente') );
				});

				$('#mdlRegistroComprobante').on('shown.bs.modal', function(e){
					get_tipos_comprobante(function(result){
						if(result['state']){
							var data = result['data'];
							var html = "";
							for(var i = 0; i < data.length; i++){
								html += "<option value='" + data[i].idTipoComprobante + "'>" + data[i].nombre + "</option>";
							}
							$('#cboTipoComprobante').html(html);
							$('#cboTipoComprobante').change();
						}
					});
				});

				$('#tblBusquedaCuentas tbody').on('click', 'tr', function(){
					var idCuenta = $(this).attr('idCuenta');

					actualizarSeleccion();
				});

				$('#cboTipoComprobante').change(function(e){
					get_series($('#cboTipoComprobante option:selected').val(), function(result){
						if(result['state']){
							var data = result['data'];
							var html = "";
							for( var i = 0; i < data.length; i++){
								html += "<option value='" + data[i].idSerie + "'>" + data[i].nombre + "</option>";
							}
							$('#cboSerie').html(html);
						}
					});
				});
			});

			function cargarClientes(){
				query = $('#txtBusquedaCliente').val();

				$.ajax({
					url : '<?php echo base_url(); ?>' + 'clientes/buscar_empresas_activas',
					type :'POST',
					dataType : 'JSON',
					data: {
						query: query
					},
					success: function(result){
						var result = result['data'];
						var cadenaHtml = '';
						for (var i = 0; i < result.length; i++) {
							cadenaHtml += "<tr class='fila' idCliente='" + result[i]['idCliente'] + "'";
							cadenaHtml += " razonSocial_empresa = '" + result[i]['razonSocial_empresa'] + "'";
							cadenaHtml += " tipo='" + result[i]['tipo'] + "'><!--td><i class='fa fa-circle estado'></i></td--><td>";
							cadenaHtml += result[i]['razonSocial_empresa'];
							cadenaHtml += "</td><td>" + result[i]['ruc'];
							cadenaHtml += "</td><td>" + result[i]['telefono'];
							cadenaHtml += "</td><td>" + result[i]['correo'];
							cadenaHtml += "</td><td><i class='fa fa-cog'></i></td></tr>";
						}
						$('#resultadoBusquedaEmpresa').html(cadenaHtml);
					}
				});
			}

			function cargarCuentas(idCliente){
				$.ajax({
					url : '<?php echo base_url(); ?>' + 'cuentas/cuentas_cliente',
					type : 'POST',
					dataType : 'JSON',
					data : {
						idCliente : idCliente
					},
					success: function(result){
						var result = result['data'];
						var cadenaHtml = '';
						for(var i = 0; i < result.length; i++){
							cadenaHtml += "<tr class='fila' idCuenta='" + result[i]['idCuenta'] + "' monto='" + result[i]['precio'] + "' ";
							cadenaHtml += "descripcion='" + result[i]['descripcion'] + "' cantidad='" + result[i]['cantidad'] + "'>";
							cadenaHtml += "<td><input type='checkbox' class='chk-det-cuenta-pago' value='' /></td>";
							cadenaHtml += "<td>" + result[i]['descripcion'] + "</td>";
							cadenaHtml += "<td>" + result[i]['cantidad'] + "</td>";
							cadenaHtml += "<td>S/." + result[i]['precio'] + "</td>";
							cadenaHtml += "</tr>";
						}
						$('#resultadoBusquedaCuentas').html(cadenaHtml);
						actualizarTotal();
					}
				});
			}
		</script>
	</body>
</html>
