<!DOCTYPE html>
<html lang='es'>
    <body>
        <header class='container'>
            <nav class='navbar navbar-default' role='navigation'>
                <div class='container'>
                    <div class='navbar-header'>
                        <a href='#' class='navbar-brand'>El Edén</a>
                    </div>
                    <div class='navbar-collapse collapse'>					
                    </div>
                </div>
            </nav>
        </header>
        <section class='container'>
            <form class='form-signin' id='frmInicioSesion'>
                <h2 class='form-signin-heading'>Ingresa tus datos</h2>
                <label for='txtUsuario' class='sr-only'>Usuario</label>
                <input type='text' id='txtUsuario' class='form-control input-top' placeholder='Usuario' required autofocus />
                <label for='txtPassword' class='sr-only'>Contraseña</label>
                <input type='password' id='txtPassword' class='form-control input-bottom' placeholder='Contraseña' required />
                <br>
                <button class='btn btn-lg btn-primary btn-block' type='submit'>Ingresar</button>
            </form>
        </section>
        <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
        <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
        <script type='text/javascript'>
            $(document).ready(function(e){
                $('#frmInicioSesion').on('submit', function(e){
                    e.preventDefault();
                    var usuario = $('#txtUsuario').val();
                    var password = $('#txtPassword').val();
                    
                    $.ajax({
                        url:'<?php echo base_url();?>usuarios/login',
                        type:'post',
                        datatype:'json',
                        data:{
                            usuario: usuario,
                            password: password
                        },
                        success: function(result){
                            var result = JSON.parse(result);
                            if(result)
                                location.href = '<?php echo base_url();?>';
                        }
                    });
                });
            });
        </script>
    </body>    
</html>