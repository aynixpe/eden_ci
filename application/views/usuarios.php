<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<title>Hospedaje El Edén</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/non-responsive.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/datepicker.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap-timepicker.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
</head>
<body>
	<section class='container'>
		<div class='row'>
			<div class='col-xs-12'>
				<div class='cuadro'>
					<h2 class='text-center'>Gestión de cuentas de usuario</h2>
					<div class='row'>
						<div class='col-xs-2'>
							<h3>Tipos
								<a href='#' class='btn btn-primary btn-sm pull-right' data-toggle='modal' data-target='#mdlRegistroTipoUsuario'>
									<i class='fa fa-plus'></i> Nuevo
								</a>
							</h3>
							<div class='row'>
								<div class='col-xs-12'>
									<div id='lstTipos' class='list-group list-habitaciones'>
									</div>
									<div id='usr-alert-tipos'>
									</div>
									<input type='hidden' id='idTipo' value='' descripcion=''>
								</div>
							</div>
						</div>
						<div class='col-xs-4'>
							<h3>Permisos
								<button type='button' id='btnEditar' class='btn btn-primary btn-sm pull-right' estado='e'><i class='fa fa-pencil'></i> Editar</button>
							</h3>
							<table class='table'>
								<thead>
									<tr>
										<th>Acción</th>
										<th class='text-center' data-toggle='tooltip' title='Buscar'><i class='fa fa-search'></i></th>
										<th class='text-center' data-toggle='tooltip' title='Agregar'><i class='fa fa-plus'></i></th>
										<th class='text-center' data-toggle='tooltip' title='Modificar'><i class='fa fa-pencil'></i></th>
										<th class='text-center' data-toggle='tooltip' title='Eliminar'><i class='fa fa-trash-o'></i></th>
									</tr>
								</thead>
								<tbody id='tblPermisos'>
									<tr>
										<td>Servicios</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
									<tr>
										<td>Operaciones</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
									<tr>
										<td>Clientes</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
									<tr>
										<td>Usuarios</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
									<tr>
										<td>Cuentas</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
									<tr>
										<td>Comprobantes</td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
										<td class='text-center'><input type='checkbox' disabled/></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class='col-xs-6'>
							<h3>Usuarios
								<a href='#' class='btn btn-primary btn-sm pull-right' id='btn-nuevo-usuario'>
									<i class='fa fa-plus'></i> Nuevo Usuario
								</a>
							</h3>
							<table class='table'>
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Usuario</th>
										<th>Tipo</th>
										<th><i class='fa fa-cogs'></i></th>
									</tr>
								</thead>
								<tbody id='tblUsuarios'>
								</tbody>
							</table>
							<div id='usr-alert-usuarios'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id='mdlRegistroTipoUsuario' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroTipoUsuarioLabel' aria-hidden='true'>
			<div class='modal-dialog modal-sm'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="RegistroTipoUsuarioLabel">Registro de Tipo de Usuario</h4>
					</div>
					<div class='modal-body'>
						<?php $this->load->view('frmRegistroTipoUsuario'); ?>
					</div>
				</div>
			</div>
		</div>
		<div id='mdlRegistroUsuario' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroUsuarioLabel' aria-hidden='true'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="RegistroUsuarioLabel">Registro de Usuario</h4>
					</div>
					<div class='modal-body'>
						<?php $this->load->view('frmRegistroUsuario'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url(); ?>js/main.js'></script>
	<script type='text/javascript'>
		var base_url = '<?php echo base_url(); ?>';
		$(document).ready(function(e){
			$('#mnu-usuarios').addClass('active');
			cargar_tipos();
			cargar_usuarios();
			$('#btn-nuevo-usuario').on('click', function(){
				get_tipos(function(result){
					var html = "";
					for(var i = 0; i < result.length; i++){
						html += "<option value='" + result[i]['idTipoUsuario'] + "'>";
						html += result[i]['descripcion'];
						html += "</option>";
					}
					$('#cboTipoUsuario').html(html);
				});
				$('#mdlRegistroUsuario').modal('show');
			});

			$('#lstTipos').on('click', 'a.list-group-item', function(e){
				e.preventDefault();
				$('#idTipo').val(this.getAttribute('idTipo'));
				cargar_permisos(this.getAttribute('idTipo'));
			}).on('mouseenter', 'a.list-group-item', function(e){
				var button = this.children[0];
				$(button).removeClass('hide');
			}).on('mouseleave', 'a.list-group-item', function(e){
				var button = this.children[0];
				$(button).addClass('hide');
			});

			$('#tblUsuarios').on('click', 'i.fa-cog', function(e){
				e.preventDefault();
				var idUsuario = this.getAttribute('idUsuario');
				get_tipos(function(result){
					var html = "";
					for(var i = 0; i < result.length; i++){
						html += "<option value='" + result[i]['idTipoUsuario'] + "'>";
						html += result[i]['descripcion'];
						html += "</option>";
					}
					$('#cboTipoUsuario').html(html);
					cargar_usuario(idUsuario, function(result){
						document.getElementById('cboTipoUsuario').value = result['idTipoUsuario'];
						$('#fru-idUsuario').val(result['idUsuario']);
						$('#txtNombre').val(result['nombre']);
						$('#txtUsuario').val(result['usuario']);
						$('#fg-pass').addClass('hide');
						$('#txtPass').prop('required', false);
						$('#mdlRegistroUsuario').modal('show');
					});
				});
			});

			$('#frmRegistroTipoUsuario').on('submit', function(e){
				e.preventDefault();

				var url = base_url + 'usuarios/' + (($('#idTipoUsuario').val() == '-1') ? 'insert_tipo' : 'update_tipo');

				$.ajax({
					url: url,
					type: 'post',
					datatype: 'json',
					data:{
						idTipo : $('#idTipoUsuario').val(),
						descripcion: $('#txtTipoUsuario').val()
					},
					success: function(result){
						var result = JSON.parse(result);
						if(result){
							$('#mdlRegistroTipoUsuario').modal('hide');
						}
					}
				});
			});
			$('#frmRegistroUsuario').on('submit', function(e){
				e.preventDefault();

				var url = ($('#fru-idUsuario').val() == '-1') ? 'insert' : 'update';

				$.ajax({
					url: base_url + 'usuarios/' + url,
					type: 'post',
					datatype: 'json',
					data: {
						idUsuario: $('#fru-idUsuario').val(),
						nombre: $('#txtNombre').val(),
						usuario: $('#txtUsuario').val(),
						password: $('#txtPass').val(),
						idTipo: $('#cboTipoUsuario').val()
					},
					success: function(result){
						var result = JSON.parse(result);
						//Mostrar mensajes
						$('#mdlRegistroUsuario').modal('hide');
					}
				});
			});
			$('#lstTipos').on('click', 'span.gestion', function(e){
				e.stopPropagation();
				var a = this.parentNode;

				var idTipo = a.getAttribute('idTipo');
				var descripcion = a.getAttribute('descripcion');

				$('#txtTipoUsuario').val(descripcion);
				$('#idTipoUsuario').val(idTipo);

				var button = document.getElementById('btnEliminar');
				$(button).removeClass('hide');

				$('#mdlRegistroTipoUsuario').modal('show');
			});
			$('#mdlRegistroTipoUsuario').on('hidden.bs.modal', function(){
				cargar_tipos();
			});
			$('#mdlRegistroUsuario').on('show.bs.modal', function(){
				/*get_tipos(function(result){
					var html = "";
					for(var i = 0; i < result.length; i++){
						html += "<option value='" + result[i]['idTipoUsuario'] + "'>";
						html += result[i]['descripcion'];
						html += "</option>";
					}
					$('#cboTipoUsuario').html(html);
				});*/
			}).on('hidden.bs.modal', function(){
				cargar_usuarios();
				limpiar_mdlRegistroUsuario();
			});
			$('#btnEditar').on('click', function(e){
				var chbs = $('#tblPermisos input[type="checkbox"]');
				if($(this).attr('estado') == 'e'){
					for(var i = 0; i < chbs.length; i ++){
						$(chbs[i]).attr('disabled', false);
					}
					$(this).attr('estado', 'g');
					$(this).html("<i class='fa fa-floppy-o'></i> Guardar");
				}else{
					var permisos = [0, 0, 0, 0, 0, 0];
					for(var i = 0; i < chbs.length; i ++){
						var index = Math.floor(i / 4);
						if($(chbs[i]).is(':checked')){
							var per = i - 4 * index;
							permisos[index] = permisos[index] | Math.pow(2, per);
						}
						$(chbs[i]).attr('disabled', true);
					}
					editar_permisos($('#idTipo').val(), permisos);
					$(this).attr('estado', 'e');
					$(this).html("<i class='fa fa-pencil'></i> Editar");
				}
			});
			$('#btnEliminar').on('click', function(e){
				$.ajax({
					url: '<?php echo base_url(); ?>usuarios/borrar_tipo',
					type: 'post',
					datatype: 'json',
					data:{
						idTipo: $('#idTipoUsuario').val()
					},
					success: function(result){
						//var result = JSON.parse(result);
						$('#mdlRegistroTipoUsuario').modal('hide');
					}
				});
			});
		});

		function editar_permisos(idTipo, permisos){
			$.ajax({
				url: '<?php echo base_url(); ?>usuarios/editar_permisos',
				type: 'post',
				datatype: 'json',
				data: {
					idTipo : idTipo,
					permisos : permisos
				},
				success: function(result){
					var result = JSON.parse(result);
					console.log(result);
				}
			});
		}

		function cargar_permisos(idTipo){
			$.ajax({
				url: '<?php echo base_url(); ?>usuarios/get_tipo',
				type: 'post',
				datatype: 'json',
				data: {
					idTipo : idTipo
				},
				success: function(result){
					var result = (JSON.parse(result))['data'][0];
					var filas = $('#tblPermisos').children();
					for(var i=0; i < filas.length; i++){
						var chbs = $(filas[i]).children();
						for(var j=1; j < chbs.length; j++){
							var x = $(chbs[j]).children();
							var y = comprobar_permiso(result[i], Math.pow(2,j-1));
							$(x[0]).prop('checked', y);
						}
					}
				}
			});
		};

		function limpiar_mdlRegistroTipoUsuario(){
			$('#txtTipoUsuario').val('');
			$('#idTipoUsuario').val('-1');

			var button = document.getElementById('btnEliminar');
			$(button).addClass('hide');
		}

		function limpiar_mdlRegistroUsuario(){
			$('#fru-idUsuario').val('-1');
			$('#txtNombre').val('');
			$('#cboTipoUsuario option:eq(0)').prop('selected', true);
			$('#txtUsuario').val('');
			$('#txtPass').val('');
			$('#fg-pass').removeClass('hide');
			$('#txtPass').prop('required', true);
		}

		function cargar_usuarios(){
			get_usuarios(function(result){
				var html = "";
				if(result['state']){
					var result = result['data'];
					for(var i = 0; i < result.length; i++){
						html += "<tr class='fila' idusuario='" + result[i]['idUsuario']  + "'><td>" +  result[i]['nombre'] + "</td>";
						html += "<td>" + result[i]['usuario'] + "</td>";
						html += "<td>" + result[i]['tipo'] + "</td>";
						html += "<td><i class='fa fa-cog gestion' idUsuario='" + result[i]['idUsuario'] + "'></i></td>";
						html += "</tr>";
					}
				}else{
					var x = '<div class="alert alert-danger alert-dismissible" role="alert">';
					x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
					x += '<span class="sr-only">Cerrar</span></button>' + result['error']['message'] + '</div>';
					$('#usr-alert-usuarios').html(x);
				}
				$('#tblUsuarios').html(html);
			});
		}

		function cargar_tipos(){
			get_tipos( function(result){
				var html = "";
				if(result['state']){
					var result = result['data'];
					for(var i = 0; i < result.length; i++){
						html += "<a href='#' class='list-group-item item-tipo'";
						html += " idtipo = '" + result[i]['idTipoUsuario'] + "' descripcion='" + result[i]['descripcion'] + "'>";
						html += result[i]['descripcion'];
						html += "<span class='fa fa-cog hide pull-right gestion'></span>";
						html += "</a>";
					}
				}else{
					var x = '<div class="alert alert-danger alert-dismissible" role="alert">';
					x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
					x += '<span class="sr-only">Cerrar</span></button>' + result['error']['message'] + '</div>';
					$('#usr-alert-tipos').html(x);
				}
				$('#lstTipos').html(html);
				limpiar_mdlRegistroTipoUsuario();
			});
		}
	</script>
</body>
</html>
