<head>
	<meta charset='utf-8'/>
	<title>Hospedaje El Edén</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/non-responsive.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />  
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/datepicker.css' />    
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap-timepicker.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
</head>