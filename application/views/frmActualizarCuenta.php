<form class='form-horizontal' id='frmActualizarCuenta'>
    <input type='hidden' id='faciddetalleoperacion' value=''>
    <input type='hidden' id='facidoperacion' value=''>
    <input type='hidden' id='facidcuenta' value=''>
    <input type='hidden' id='facfechaingreso' value=''>
    <input type='hidden' id='facprecioservicio' value=''>
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='checkbox'>
                    <label>
                        <input type='checkbox' id='facchbegreso'>
                        Egreso
                    </label>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-6'>
                <div class='form-group'>
                    <label class='col-xs-4 control-label'>Fecha</label>
                    <div class='col-xs-8'>
                        <input type='text' class='form-control input-sm' id='factxtfechaegreso' disabled>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-xs-4 control-label'>Hora</label>
                    <div class='col-xs-8'>
                        <div class="input-group input-group-sm">
                            <input id="factxthoraegreso" class="form-control input-sm" type="text" disabled>
                            <span class="input-group-btn">
                                <button class='btn btn-sm btn-default' type='button'><i class="fa fa-clock-o"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-xs-6'>
                <div class='form-group'>
                    <label class='col-xs-4 control-label'>Cantidad</label>
                    <div class='col-xs-8'>
                        <input type='number' class='form-control input-sm' id='factxtcantidad' min='1'>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-xs-4 control-label'>Monto</label>
                    <div class='col-xs-6'>
                        <input type='text' class='form-control input-sm' id='factxtmonto'>
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-12'>
                <input type='button' class='btn btn-primary' id='facbtncalcdias' value='Calcular Días'>
                <div class='pull-right'>
                    <input type='button' class='btn btn-danger' value='Cancelar' id='facbtncancelar'>
                    <input type='submit' class='btn btn-success' value='Guardar'>
                </div>
            </div>
        </div>
    </div>
</form>
