<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<title>Hospedaje El Edén</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/non-responsive.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/datepicker.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap-timepicker.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
</head>
<body>
	<section class='container'>
		<div class='row'>
			<section class='col-xs-12'>
				<div class='cuadro'>
					<h2 class='text-center'>Registro de Ventas</h2>
					<form id='registro-venta' role='form'>
						<div class='row'>
							<div class='col-xs-6 borde-derecha'>
								<div class='form-horizontal'>
									<div class='form-group'>
										<label for='txtCliente' class='col-xs-4 control-label'><code>*</code>Cliente</label>
										<div class='col-xs-8'>
											<div class='input-group input-group-sm'>
												<input id='txtCliente' type='text' class='form-control' disabled placeholder='Cliente' required />
												<span class='input-group-btn'>
													<button class='btn btn-primary' type='button' id='btnBuscarCliente'><i class='fa fa-search'></i></button>
												</span>
											</div>
										</div>
									</div>
									<input type='hidden' id='idCliente' />
									<div class='form-group'>
										<div class='col-xs-offset-4 col-xs-7'>
											<button type='button' class='btn btn-sm btn-primary col-xs-6'>Ver historial</button>
											<button type='button' class='btn btn-sm btn-primary col-xs-4 col-xs-offset-1'
													data-toggle='modal' data-target='#mdlRegistroCliente'>
												<i class='fa fa-plus'></i> Nuevo
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class='col-xs-6'>
								<div class='form-horizontal'>
									<div class='form-group'>
										<label for='txtFecha' class='col-xs-4 control-label'><code>*</code>Fecha</label>
										<div class='col-xs-5'>
											<input id='txtFecha' class="form-control input-sm fecha" type="text" required>
										</div>
									</div>
									<div class='form-group'>
										<label for='txtHora' class='col-xs-4 control-label'><code>*</code>Hora</label>
										<div class='col-xs-4'>
											<!--input id='txtHora' type='time' class='form-control input-sm' value='00:00' required /-->
											<div class="input-group input-group-sm">
												<input id="txtHora" class="form-control input-sm" type="text">
												<span class="input-group-btn">
													<button class='btn btn-sm btn-default' type='button'><i class="fa fa-clock-o"></i></button>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class='row'>
							<div class='form-horizontal col-xs-12'>
								<div class='form-group'>
									<label for='txtObservacion' class='col-xs-2 control-label'>Observación</label>
									<div class='col-xs-10'>
										<input id='txtObservacion' type='text' class='form-control input-sm' placeholder='Observación' />
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class='row'>
							<div class='col-xs-6'>
								<div class='form-horizontal'>
									<div class='form-group'>
										<label for='txtHuesped' class='col-xs-4 control-label'><code>*</code>Huesped</label>
										<div class='col-xs-8'>
											<div class='input-group input-group-sm'>
												<input type='hidden' id='idHuesped' />
												<input id='txtHuesped' disabled type='text' class='form-control' placeholder='Huesped' required />
												<span class='input-group-btn'>
													<button class='btn btn-primary' type='button' id='btnBuscarHuesped'><i class='fa fa-search'></i></button>
												</span>
											</div>
										</div>
									</div>
									<div class='form-group'>
										<label for='cboServicio' class='col-xs-4 control-label'><code>*</code>Tipo Servicio</label>
										<div class='col-xs-8'>
											<div class="dropdown">
												<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
													Todos
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" id='cboServicio'>
												</ul>
											</div>
										</div>
									</div>
									<div class='form-group'>
										<label class='col-xs-4 control-label'><code>*</code>Habitación</label>
										<div class='col-xs-8'>
											<div id='lstHabitaciones' class='list-group lista-habitaciones'>
											</div>
										</div>
										<input type='hidden' id='idHabitacion' value='' descripcion=''>
									</div>
									<div class='form-group'>
										<label for='txtNumPersonas' class='col-xs-4 control-label'><code>*</code>Personas</label>
										<div class='col-xs-4'>
											<input id='txtNumPersonas' type='number' class='form-control input-sm' min='1' value='1' required />
										</div>
									</div>
									<div class='form-group'>
										<label class='col-xs-4 control-label'><code>*</code>Modalidad</label>
										<div class='col-xs-8'>
											<div class='radio col-xs-4'>
												<label>
													<input type='radio' name='rbModalidadVenta' id='rbDiasVenta' value='d' checked />
													Días
												</label>
											</div>
											<div class='radio col-xs-4'>
												<label>
													<input type='radio' name='rbModalidadVenta' id='rbHorasVenta' value='h' />
													Horas
												</label>
											</div>
										</div>
									</div>
									<div class='form-group'>
										<label for='txtCantidad' class='col-xs-4 control-label'><code>*</code>Cant. y precio</label>
										<div class='col-xs-4'>
											<input id='txtCantidad' type='number' class='form-control input-sm' min='1' value='1' required />
										</div>
										<div class='col-xs-4'>
											<input id='txtPrecio' type='text' class='form-control input-sm' placeholder='S/. 30.00' required />
										</div>
									</div>
									<div class='form-group'>
										<label for='txtMotivo' class='col-xs-4 control-label'>Motivo</label>
										<div class='col-xs-8'>
											<input id='txtMotivo' type='text' class='form-control input-sm' placeholder='Motivo' />
										</div>
									</div>
									<div class='form-group'>
										<label for='txtFechaEntrada' class='col-xs-4 control-label'><code>*</code>Entrada</label>
										<div class='col-xs-4'>
											<input id='txtFechaEntrada' class="form-control input-sm fecha" type="text" required>
										</div>
										<div class='col-xs-4'>
											<!--input id='txtHora' type='time' class='form-control input-sm' value='00:00' required /-->
											<div class="input-group input-group-sm">
												<input id="txtHoraEntrada" class="form-control input-sm" type="text">
												<span class="input-group-btn">
													<button class='btn btn-sm btn-default' type='button'><i class="fa fa-clock-o"></i></button>
												</span>
											</div>
										</div>
									</div>
									<div class='form-group'>
										<label for='txtObservacionHuesped' class='col-xs-4 control-label'>Observación</label>
										<div class='col-xs-8'>
											<input id='txtObservacionHuesped' type='text' class='form-control input-sm' placeholder='Observación' />
										</div>
									</div>
									<div class='form-group'>
										<div class='col-xs-offset-8 col-xs-4'>
											<button type='button' id='btnVentaAgregar' class='btn btn-primary col-xs-12'>
												Agregar <i class='fa fa-caret-right'></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class='col-xs-6'>
								<ul id='lstDetalleVenta' class="list-group lista-detalles">
								</ul>
								<div class='row'>
									<div class='col-xs-10'>
										<strong>Total: S/.</strong> <label id='totalVenta'></label>
									</div>
								</div>
								<div class="row">
									<div class='col-xs-7'>
										<div id='msgVenta'>
										</div>
									</div>
									<div class='col-xs-5'>
										<button class='btn btn-danger col-xs-5'>Cancelar</button>
										<button id='btnVentaAceptar' type='submit' class='btn btn-success col-xs-5 col-xs-offset-2'>Aceptar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					</br>
				</div>
			</section>
		</div>
	</section>
	<footer>
		<br>
	</footer>
	<!--   MODALES    -->
	<div id='mdlBuscarCliente' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='BusquedaClienteLabel' aria-hidden='true'>
		<div class='modal-dialog'>
			<div class='modal-content'>
				<div class='modal-header'>
					<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
					<h4 class='modal-title' id='BusquedaClienteLabel'>Búsqueda de Cliente</h4>
				</div>
				<div class='modal-body'>
					<div class='row'>
						<div class="col-xs-12">
							<form id='jsFrmBuscarClientes'>
								<div class="input-group input-group-sm">
									<input id='txtBusquedaCliente' type='text' class='form-control' placeholder='Cliente' required />
									<span class='input-group-btn'>
										<button class='btn btn-primary' id='jsBuscarClientes' type='submit'><i class='fa fa-search'></i></button>
									</span>
								</div>
							</form>
						</div>
					</div>
					<br>
					<div class='row'>
						<div class='col-xs-12'>
							<table class='table table-condensed' id='tblBusquedaCliente'>
								<thead>
									<tr>
										<th>Cliente</th>
										<th>DNI</th>
										<th>RUC</th>
									</tr>
								</thead>
								<tbody id='jsResultadoBusquedaCliente'>
								</tbody>
							</table>
						</div>
					</div>
					<input type='hidden' id='tipoBusqueda' value='-1'/>
				</div>
			</div>
		</div>
	</div>

	<div id='mdlRegistroCliente' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroClienteLabel' aria-hidden='true'>
	    <div class='modal-dialog'>
	        <div class='modal-content'>
	            <div class='modal-header'>
	                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	                <h4 class="modal-title" id="RegistroClienteLabel">Registro de cliente</h4>
	            </div>
	            <div class='modal-body'>
	                <div class='panel-group' id='pnlRegistroCliente'>
	                    <div class='panel panel-default'>
	                        <div class='panel-heading'>
	                            <h4 class='panel-title'>
	                                <a data-toggle='collapse' data-parent='#pnlRegistroCliente' href='#collapsePersona'>Persona</a>
	                            </h4>
	                        </div>
	                        <div id='collapsePersona' class='panel-collapse collapse'>
	                            <div class='panel-body'>
	                                <?php $this->load->view('frmRegistroCliente'); ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class='panel panel-default'>
	                        <div class='panel-heading'>
	                            <h4 class='panel-title'>
	                                <a data-toggle='collapse' data-parent='#pnlRegistroCliente' href='#collapseEmpresa'>Empresa</a>
	                            </h4>
	                        </div>
	                        <div id='collapseEmpresa' class='panel-collapse collapse'>
	                            <div class='panel-body'>
	                                <?php $this->load->view('frmRegistroEmpresa'); ?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>


	<!-- FIN MODALES -->
	<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-datepicker.js'></script>
	<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-timepicker.js'></script>
	<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-typeahead.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url(); ?>js/main.js'></script>
	<script type='text/javascript'>
		$(document).ready(function(){
			procedencia($('#txtProcedencia'), $('#idProcedenciaPersona'));
			procedencia($('#txtProcedencia-empresa'), $('#idProcedenciaEmpresa'));

			timepicker($('#txtHora'));
			timepicker($('#txtHoraEntrada'));

			datepicker($('#txtFecha'));
			datepicker($('#txtFechaEntrada'));

			/**HACIA ARRIBA ESTÁ MODIFICADO **/
			/** AJAX
			**/
			document.oncontextmenu = function(){
				return false;
			};

			cargarTiposHabitacion();
			cargarTodasHabitaciones();

			$("#cboServicio").on('click', 'a', function(e){
				e.preventDefault();
				cargarHabitaciones($(this));
			});

			function cargarHabitaciones(e){
				var tipo = e.text();
				var idTipo = e.attr('idTipoHabitacion');
				$.ajax({
					url : "<?php echo base_url().'habitaciones/obtener_habitaciones_tipo'; ?>",
					type: 'post',
					dataType : 'json',
					data:{
						idTipo : idTipo,
						estado : 1 // disponibles
					},
					success: function(result){
						var result = result['data'];
						var x = "";
						for(i = 0; i < result.length; i++){
							x += "<div class='list-group-item lista-radio'>";
							x += "<div class='radio'> <label class='item-radio'>";
							x += "<input type='radio' name='itmHabitacion' descripcion = '" + result[i].descripcion + "'";
							x += " precio='" + result[i].precio +"' tipoPrecio='" + result[i].tipoPrecio;
							x += "' id='serv" + result[i].idServicio + "' value='" + result[i].idServicio + "'>";
							x += result[i].descripcion;
							x += "</label> </div> </div>";
						}
						$('#lstHabitaciones').html(x);
						$('#dropdownMenu1').html(tipo + " <span class='caret'></span>");
					}

				});
			}
			$('#lstHabitaciones').on('click', 'label', function(e){
				var x = $(this).children().get(0).value;
				var precio = $(this).children().get(0).getAttribute('precio');
				var tipoPrecio = $(this).children().get(0).getAttribute('tipoPrecio');
				$('#idHabitacion').val(x);
				var habitacion = $(this).children().get(0).getAttribute('descripcion');
				$('#idHabitacion').attr('descripcion', habitacion);
				if(precio == 0 || precio == 'null'){
					$('#txtPrecio').val(tipoPrecio * $('#txtCantidad').val());
				}else{
					$('#txtPrecio').val(precio * $('#txtCantidad').val());
				}
			});

			$('#txtCantidad').on('change', function(e){
				if( $('#idHabitacion').val() != 'null'){
					var habitacion = $('input[name=itmHabitacion]:checked');

					var precio = habitacion[0].getAttribute('precio');
					if(precio == 0 || precio == 'null'){
						precio = habitacion[0].getAttribute('tipoPrecio');
					}
					$('#txtPrecio').val(precio * $(this).val());

				}
			});

			$('#jsFrmBuscarClientes').on('submit', function(e){
				e.preventDefault();

				query = $('#txtBusquedaCliente').val();

				var tipo = $('#tipoBusqueda').val();
				var url = (tipo == '1') ? 'clientes/buscar_clientes_activos' : 'clientes/buscar_personas_activas';

				$.ajax({
					url: '<?php echo base_url();?>' + url,
					type:'post',
					dataType: 'json',
					data:{
						query : query
					},
					success: function(result){
						var result = result['data'];
						var cadenaHtml = '';
						for (var i = 0; i < result.length; i++) {
							var cliente = (result[i]['nombres_cliente'] == null) ? result[i]['razonSocial_empresa'] : result[i]['nombres_cliente'];
							cadenaHtml += "<tr class='fila' idCliente='" + result[i]['idCliente'] + "'";
							cadenaHtml += " nombreCliente = '" + cliente + "'";
							cadenaHtml += " tipo='" + result[i]['tipo'] + "'><td>";
							cadenaHtml += cliente;
							cadenaHtml += "</td><td>" + result[i]['documento'];
							cadenaHtml += "</td><td>" + result[i]['ruc'] + "</td></tr>";
						};
						$('#jsResultadoBusquedaCliente').html(cadenaHtml);
					}
				});
			});

			$('#tblBusquedaCliente tbody').on('click', 'tr', function(){
				var idCliente = $(this).attr('idCliente');
				var nombreCliente = $(this).attr('nombreCliente');
				var cliente = $(this).children();

				var tipoCliente = this.getAttribute('tipo');
				var tipo = $('#tipoBusqueda').val();
				if(tipo == '1'){
					$("#idCliente").val(idCliente);
					$("#txtCliente").val(nombreCliente);
				}
				if(tipoCliente == 'P'){
					$("#idHuesped").val(idCliente);
					$("#txtHuesped").val(nombreCliente);
				}
				$('#mdlBuscarCliente').modal('hide');
			});

			$('#mdlBuscarCliente').on('hidden.bs.modal', function(e){
				$('#jsResultadoBusquedaCliente').html('');
			});

			$('#btnVentaAgregar').on('click', function(e){
				var idHabitacion = $('#idHabitacion').val();
				if(idHabitacion != ''){
					var itemsVenta = $('#lstDetalleVenta').children();
					var flag = false;
					var i = 0;
					while( !flag && i < itemsVenta.length){
						flag = (itemsVenta.get(i).getAttribute('idHabitacion') == idHabitacion) ? true : false;
						i++;
					}

					if(!flag){
						var idHuesped = $('#idHuesped').val();
						if(idHuesped != ''){
							var habitacion = document.getElementById('idHabitacion').getAttribute('descripcion');
							var numPersonas = $('#txtNumPersonas').val();
							var modalidad = (document.getElementById('rbDiasVenta').checked) ? 'D' : 'H';
							var cantidad = $('#txtCantidad').val();
							var precio = $('#txtPrecio').val();
							var motivo = $('#txtMotivo').val();
							var fechaEntrada = document.getElementById('txtFechaEntrada').value;
							var horaEntrada = document.getElementById('txtHoraEntrada').value;
							var observacion = $ ('#txtObservacionHuesped').val();

							var html = "<li class='list-group-item'";
								html += " idhuesped='" + idHuesped + "'";
								html += " idhabitacion='" + idHabitacion + "'";
								html += " numpersonas='" + numPersonas + "'";
								html += " modalidad='" + modalidad + "'";
								html += " cantidad='" + cantidad + "'";
								html += " precio='" + precio + "'";
								html += " motivo='" + motivo + "'";
								html += " fechaEntrada='" + fechaEntrada + "'";
								html += " horaEntrada='" + horaEntrada + "'";
								html += " observacion='" + observacion + "'>";
								html += "<span class='deleteItemVenta fa fa-minus-square'></span> " + habitacion + " - S/." + precio + "</li>";

							var $lista = $('#lstDetalleVenta');
							$lista.append(html);

							actualizarTotal();
						}
					}
				}else{
					$('#msgVenta').empty();
				}
			});

			$('#lstDetalleVenta').on('click', 'span', function(e){
				var li = $(this).parents().get(0);
				li.remove();
				actualizarTotal();
			});

			$('#registro-venta').on('submit', function(e){
				e.preventDefault();
				var idCliente = document.getElementById('idCliente').value;
				var tipo = 'V';
				var monto = $('#totalVenta').text();
				var observacion = document.getElementById('txtObservacion').value;
				var fecha = document.getElementById('txtFecha').value;
				var hora = document.getElementById('txtHora').value;

				var itemsVenta = $('#lstDetalleVenta').children();
				var detallesVenta = {};
				for (var i = 0; i < itemsVenta.length; i++) {
					var detalleVenta = {
						'idHuesped' : parseInt(itemsVenta.get(i).getAttribute('idhuesped')),
						'idHabitacion' : parseInt(itemsVenta.get(i).getAttribute('idhabitacion')),
						'numPersonas' : parseInt(itemsVenta.get(i).getAttribute('numpersonas')),
						'modalidad' : itemsVenta.get(i).getAttribute('modalidad'),
						'cantidad' : parseInt(itemsVenta.get(i).getAttribute('cantidad')),
						'precio' : parseFloat(itemsVenta.get(i).getAttribute('precio')),
						'motivo' : itemsVenta.get(i).getAttribute('motivo'),
						'fechaEntrada' : itemsVenta.get(i).getAttribute('fechaEntrada'),
						'horaEntrada' : itemsVenta.get(i).getAttribute('horaEntrada'),
						'observacion' : itemsVenta.get(i).getAttribute('observacion')
					};
					detallesVenta[i] = detalleVenta;
				};

				var detalles = JSON.stringify(detallesVenta);

				$.ajax({
					url : "<?php echo base_url().'operaciones/insertar_venta'; ?>",
					dataType : 'json',
					data:{
						idCliente : idCliente,
						tipo : tipo,
						monto : monto,
						observacion : observacion,
						fecha : fecha,
						hora : hora,
						detalles : detalles
					},
					type : 'POST',
					success: function(result){
						if(result['state']){
							limpiarVenta();
							var x = '<div class="alert alert-success alert-dismissible" role="alert">';
							x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
							x += '<span class="sr-only">Cerrar</span></button>Venta registrada</div>';
							$('#msgVenta').html(x);
						}else{
							var x = '<div class="alert alert-danger alert-dismissible" role="alert">';
							x += '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>';
							x += '<span class="sr-only">Cerrar</span></button><h4>Error</h4>' + result['error']['message'] + '</div>';
							$('#msgVenta').html(x);
						}
					}
				});
			});

			$('#mnu-operaciones').addClass('active');
		});



		function actualizarTotal(){
			var total = 0;
			var itemsVenta = $('#lstDetalleVenta').children();
			for (var i = itemsVenta.length - 1; i >= 0; i--) {
				total += parseFloat(itemsVenta.get(i).getAttribute('precio'));
			};
			$('#totalVenta').text(total);
		}

		function limpiarVenta(){
			$('#txtCliente').val('');
			$('#idCliente').val('');
			$('#txtCantidad').val('1');
			$('#txtObservacion').val('');
			$('#txtHuesped').val('');
			$('#idHuesped').val('');
			cargarTiposHabitacion();
			cargarTodasHabitaciones();
			$('#idHabitacion').val('');
			$('#txtNumPersonas').val('1');
			$('#txtPrecio').val('');
			$('#txtMotivo').val('');
			$('#txtObservacionHuesped').val('');
			$('#lstDetalleVenta').html('');
			actualizarTotal();
		}

		function cargarTiposHabitacion(){
			$.ajax({
				url : "<?php echo base_url().'habitaciones/obtener_tipos_habitacion'; ?>",
				dataType : 'json',
				type : 'POST',
				success: function(result){
					var result = result['data'];
					var x = "<li class='itmCboServicios' role='presentation'><a href='#' role='menuitem' tabindex='-1' idTipoHabitacion='0'>Todos</a></li>";
					for(i = 0; i < result.length; i++){
						x += "<li class='itmCboServicios' role='presentation'><a href='#' role='menuitem' tabindex='-1' idTipoHabitacion='";
						x += result[i].idTipo + "'>" + result[i].descripcion + "</a></li>";
					}
					$('#cboServicio').html(x);
				}
			});
		}

		function cargarTodasHabitaciones(){
			$.ajax({
				url : "<?php echo base_url().'habitaciones/obtener_habitaciones_tipo'; ?>",
				type : 'POST',
				dataType : 'json',
				data:{
					idTipo : 0,
					estado : 1 //disponibles
				},
				success: function(result){
					var result = result['data'];
					var x = "";
					for(i = 0; i < result.length; i++){
						x += "<div class='list-group-item lista-radio'>";
						x += "<div class='radio'> <label class='item-radio'>";
						x += "<input type='radio' name='itmHabitacion' descripcion = '" + result[i].descripcion + "'";
						x += " precio='" + result[i].precio +"' tipoPrecio='"
						x += result[i].tipoPrecio +"' id='serv" + result[i].idServicio + "' value='" + result[i].idServicio + "'>";
						x += result[i].descripcion;
						x += "</label> </div> </div>";
					}
					$('#lstHabitaciones').html(x);
				}
			});
		}
	</script>
</body>
</html>
