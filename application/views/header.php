<header class='container'>
	<nav class='navbar navbar-default' role='navigation'>
		<div class='container'>
			<div class='navbar-header'>
				<a href='#' class='navbar-brand'>El Edén</a>
			</div>
			<div class='navbar-collapse collapse'>
				<ul class='nav navbar-nav'>
					<li id='mnu-operaciones'><a href='<?php echo base_url(); ?>operaciones'>Operaciones</a></li>
					<li id='mnu-clientes'><a href='<?php echo base_url(); ?>clientes/gestion'>Clientes</a></li>
					<li id='mnu-empresas'><a href='<?php echo base_url(); ?>empresas/gestion'>Empresas</a></li>
					<li id='mnu-usuarios'><a href='<?php echo base_url(); ?>usuarios/gestion'>Usuarios</a></li>
					<li id='mnu-habitaciones'><a href='<?php echo base_url(); ?>habitaciones/gestion'>Habitaciones</a></li>
					<li id='mnu-caja'><a href='<?php echo base_url(); ?>caja/gestion'>Caja</a></li>
					</ul>
					<ul class='nav navbar-nav navbar-right'>
						<li class='dropdown'>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $usuario;?><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href='<?php echo base_url();?>usuarios/logout_ci'>Salir</a></li>
							</ul>
						</li>
					</ul>
			</div>
		</div>
	</nav>
</header>
