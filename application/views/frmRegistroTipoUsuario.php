<form class='form-horizontal' role='form' id='frmRegistroTipoUsuario'>
    <input id='idTipoUsuario' type='hidden' id='-1'/>
    <div class='form-group'>
        <label for='txtTipoUsuario' class='col-xs-3 control-label'>Tipo</label>
        <div class='col-xs-9'>
            <input id='txtTipoUsuario' type='text' class='form-control input-sm' placeholder='Tipo' required/>
        </div>
    </div>
    <div id='alertRegistroTipoUsuario'>
    </div>
    <div class='form-group'>
        <div class='col-xs-12'>
            <button type='button' id='btnEliminar' class='btn btn-danger btn-sm hide'><i class='fa fa-trash-o'></i> Borrar</button>
            <input type='button' data-dismiss='modal' value='Cancelar' class='btn btn-danger btn-sm' />
            <input id='btnRegistrarTipoUsuario' type='submit' value='Registrar' class='btn btn-primary btn-sm ' />
        </div>
    </div>
</form>
