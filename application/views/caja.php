<!DOCTYPE html>
<html lang='es'>
	<body>
		<section class='container'>
			<div class='row'>
				<div class='col-xs-12'>
					<div class='cuadro'>
						<h2 class='text-center'>Caja</h2>
						<div class='row'>
							<div class='col-xs-6'>
								<div class='row'>
									<div class='col-xs-8'>
										<form class='form' id='frmBuscarCuentas'>
											<div class="input-group input-group-sm">
												<input id='txtBusquedaCuenta' type='text' class='form-control' placeholder='Buscar...' required />
												<span class='input-group-btn'>
													<button class='btn btn-primary' id='jsBuscarClientes' type='submit'><i class='fa fa-search'></i></button>
												</span>
											</div>
										</form>
									</div>
								</div>
								<div class='row'>
									<div class='col-xs-12'>
										<table class='table table-condensed' id='tblBusquedaCliente'>
											<thead>
												<tr>
													<th>Cliente</th>
													<th>Servicio</th>
													<th>Total</th>
												</tr>
											</thead>
											<tbody id='resultadoBusquedaCuenta'>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class='col-xs-6'>
								<div class='row'>
									<div class='col-xs-12'>
										<h3 id='h3-cuentas' class='no-margin-top'>Cuentas</h3>
									</div>
								</div>
								<div class='row'>
									<div class='col-xs-12'>
										<table class='table table-condensed' id='tblBusquedaCliente'>
											<thead>
												<tr>
													<th><i class='fa fa-check-square-o'></i></th>
													<th>Servicio</th>
													<th>Cantidad</th>
													<th>Total</th>
												</tr>
											</thead>
											<tbody id='resultadoBusquedaDetalles'>
											</tbody>
										</table>
									</div>
								</div>
								<div class='row'>
									<div class='col-xs-4'><b id='total'>Total: S/.0.00</b></div>
									<div class='col-xs-4'><b id='totalSelect'>Seleccionado: S/.0.00</b></div>
									<div class='col-xs-4'>
										<button id='btnPagar' type='button' class='btn btn-success btn-sm col-xs-12'>Pagar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- MODALES-->
		<div id='mdlRegistroComprobante' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='RegistroComprobanteLabel' aria-hidden='true'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="RegistroComprobanteLabel">Registro de comprobante</h4>
					</div>
					<div class='modal-body'>
						<form class='form-inline' id='frmRegistroComponente'>
							<div class='row'>
								<label class='control-label col-xs-2'>Comprobante</label>
								<div class='col-xs-2'>
									<select id='cboTipoComprobante' class='form-control input-sm' style='width:100%;'>
									</select>
								</div>
								<div class='col-xs-3'>
									<input type='text' class='form-control input-sm' disabled>
								</div>
								<div class='col-xs-3 col-xs-offset-2'>
									<div class="input-group bootstrap-timepicker">
										<input id="txtHora" class="form-control input-sm" type="text">
										<span class="input-group-btn">
											<button class='btn btn-sm btn-default' type='button'><i class="fa fa-clock-o"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class='row'>
								<div class='col-xs-12'>
									<h5>Ítems</h5>
									<table class='table table-hover'>
										<thead>
											<tr>
												<th>Ítem</th>
												<th>Descripción</th>
												<th>Cantidad</th>
												<th>Monto</th>
											</tr>
										</thead>
										<tbody id='tblItemsComprobante'>
										</tbody>
									</table>
								</div>
							</div>
							<div class='row'>
								<div class='col-xs-2 col-xs-offset-10'>
									<button type='submit' class='btn btn-primary btn-sm col-xs-12'>Pagar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- FIN MODALES-->
		<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-timepicker.js'></script>
		<script type='text/javascript'>
			$(document).ready(function(){
				$('#mnu-caja').addClass('active');
				$('#txtHora').timepicker({
					minuteStep: 1,
					secondStep: 1,
					showInputs: false,
					disableFocus: true,
					showSeconds: true,
					template: 'dropdown'
				});

				$('#frmBuscarCuentas').on('submit', function(e){
					e.preventDefault();
					var query = $('#txtBusquedaCuenta').val();
					$.ajax({
						url : '<?php echo base_url();?>cuentas/get_cuentas',
						type : 'post',
						datatype : 'json',
						data : {
							query : query
						},
						success : function(result){
							var result = JSON.parse(result);
							var cadenaHtml = '';
							for (var i = 0; i < result.length; i++) {
								var cliente = (result[i]['nombres_cliente'] == null) ? result[i]['razonSocial_empresa'] : result[i]['nombres_cliente'] + ', ' + result[i]['apellidos_cliente'];
								cadenaHtml += "<tr class='fila' idCuenta='" + result[i]['idCuenta'] + "'";
								cadenaHtml += " nombreCliente = '" + cliente + "'><td>";
								cadenaHtml += cliente;
								cadenaHtml += "</td><td>" + result[i]['descripcion'];
								cadenaHtml += "</td><td>S/." + result[i]['monto'] + "</td></tr>";
							};
							$('#resultadoBusquedaCuenta').html(cadenaHtml);
						}
					});
				});

				$('#resultadoBusquedaCuenta').on('click', 'tr', function(e){
					e.preventDefault();
					var idCuenta = $(this).attr('idCuenta');

					var query = $('#txtBusquedaCuenta').val();
					$.ajax({
						url : '<?php echo base_url();?>cuentas/detalles_cuenta',
						type : 'post',
						datatype : 'json',
						data : {
							idCuenta : idCuenta
						},
						success : function(result){
							var result = JSON.parse(result);
							var cadenaHtml = '';
							for (var i = 0; i < result.length; i++) {
								cadenaHtml += "<tr class='fila' idDetalleOperacion='" + result[i]['idDetalleOperacion'] + "' ";
								cadenaHtml += "monto='" + result[i]['precio'] + "' descripcion='" + result[i]['descripcion'] + "' ";
								cadenaHtml += "cantidad='" + result[i]['cantidad'] + "'><td>";
								cadenaHtml += "<input type='checkbox' class='chk-det-cuenta-pago' value='' /></td><td>";
								cadenaHtml += result[i]['descripcion'];
								cadenaHtml += "</td><td>" + result[i]['cantidad'];
								cadenaHtml += "</td><td class='hola'>S/." + result[i]['precio'] + "</td></tr>";
							};
							$('#resultadoBusquedaDetalles').html(cadenaHtml);
							actualizarTotal();
						}
					});
				});

				$('#resultadoBusquedaDetalles').on('click', 'input', function(e){
					actualizarSeleccion();
				});

				$('#btnPagar').on('click', function(e){
					var j = 1;
					var cadenaHtml = "";
					var items = $('#resultadoBusquedaDetalles').children();
					for (var i = 0; i < items.length; i++) {
						var tds = $(items.get(i)).children();
						var chb = $(tds.get(0)).children().get(0);
						if( $(chb).is(':checked') ){
							var fila = items.get(i);
							cadenaHtml += "<tr class='fila' idDetalleOperacion='" + fila.getAttribute('idDetalleOperacion') + "' ";
							cadenaHtml += "monto='" + fila.getAttribute('monto') + "' descripcion='" + fila.getAttribute('descripcion') + "' ";
							cadenaHtml += "cantidad='" + fila.getAttribute('cantidad') + "'><td>";
							cadenaHtml += j + "</td><td>";
							cadenaHtml += fila.getAttribute('descripcion');
							cadenaHtml += "</td><td>" + fila.getAttribute('cantidad');
							cadenaHtml += "</td><td>S/." + fila.getAttribute('monto') + "</td></tr>";
							j++;
						}
					};
					tiposComprobante();
					$('#tblItemsComprobante').html(cadenaHtml);
					$('#mdlRegistroComprobante').modal('show');
				});
				$('#frmRegistroComponente').on('submit', function(e){
					e.preventDefault();
					var idTipoComprobante = $('#cboTipoComprobante').val();
					console.log(idTipoComprobante);
				});
			});

			function tiposComprobante(){
				$.ajax({
					url:'<?php echo base_url();?>comprobantes/get_tipos',
					type:'post',
					datatype:'json',
					success:function(result){
						var result = JSON.parse(result);
						var cadenaHtml = "";
						for (var i = 0; i < result.length; i++) {
							cadenaHtml += "<option value='" + result[i]['idTipoComprobante'] + "'>" + result[i]['nombre'] + "</option>";
						};
						$('#cboTipoComprobante').html(cadenaHtml);
					}
				});
			}

			function actualizarTotal(){
				var total = 0;
				var itemsVenta = $('#resultadoBusquedaDetalles').children();
				for (var i = itemsVenta.length - 1; i >= 0; i--) {
					total += parseFloat(itemsVenta.get(i).getAttribute('monto'));
				};
				$('#total').text("Total: S/." + total);
				actualizarSeleccion();
			}

			function actualizarSeleccion(){
				var total = 0;
				var itemsVenta = $('#resultadoBusquedaDetalles').children();
				for (var i = itemsVenta.length - 1; i >= 0; i--) {
					/*total += parseFloat(itemsVenta.get(i).getAttribute('monto'));
					console.log(total);*/
					var tds = $(itemsVenta.get(i)).children();
					var chb = $(tds.get(0)).children().get(0);
					if( $(chb).is(':checked') ){
						total += parseFloat(itemsVenta.get(i).getAttribute('monto'));
					}
				};
				$('#totalSelect').text("Total: S/." + total);
			}
		</script>
	</body>
</html>
