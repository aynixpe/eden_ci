<form class='form-horizontal' role='form' id='frmRegistroPersona'>
    <div class='form-group'>
        <label for='cboTipoDocumento' class='col-xs-4 control-label'>Tipo Doc.</label>
        <div class='col-xs-3'>
            <select id='cboTipoDocumento' class='form-control input-sm'>
                <option value='1'>DNI</option>
                <option value='2'>Pasaporte</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='txtNumDoc' class='col-xs-4 control-label'><code>*</code>N° Documento</label>
        <div class='col-xs-3'>
            <input id='txtNumDoc' type='text' class='form-control input-sm' placeholder='00000000000' required />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtRUC' class='col-xs-4 control-label'>RUC</label>
        <div class='col-xs-3'>
            <input id='txtRUC' type='text' class='form-control input-sm' placeholder='00000000000' />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtNombres' class='col-xs-4 control-label'><code>*</code>Nombres</label>
        <div class='col-xs-6'>
            <input type='text' id='txtNombres' class='form-control input-sm' placeholder='Nombres' required />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtApellidos' class='col-xs-4 control-label'><code>*</code>Apellidos</label>
        <div class='col-xs-6'>
            <input type='text' id='txtApellidos' class='form-control input-sm' placeholder='Apellidos' required />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtProcedencia' class='col-xs-4 control-label'><code>*</code>Procedencia</label>
        <div class='col-xs-7'>
            <input id='txtProcedencia' type='search' autocomplete='off' class='form-control input-sm' placeholder='Distrito o país' required/>
            <input type='hidden' id='idProcedenciaPersona' value='-1' />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtTelefono' class='col-xs-4 control-label'>Teléfono</label>
        <div class='col-xs-3'>
            <input type='tel' id='txtTelefono' class='form-control input-sm' placeholder='000-000000' />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtEmail' class='col-xs-4 control-label'>Correo</label>
        <div class='col-xs-5'>
            <input type='email' id='txtCorreo' class='form-control input-sm' placeholder='xxxxxx@xxxxxx.xxx' />
        </div>
    </div>
    <div id='alertRegistroPersona'>
    </div>
    <div class='form-group'>
        <div class='col-xs-3 col-xs-offset-4'>
            <input type='button' data-dismiss='modal' value='Cancelar' class='btn btn-danger btn-sm col-xs-12' />
        </div>
        <div class='col-xs-3'>
            <input id='btnRegistrarPersona' type='submit' value='Registrar' class='btn btn-primary btn-sm col-xs-12' />
        </div>
    </div>
</form>
