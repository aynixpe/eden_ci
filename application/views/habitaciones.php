<!DOCTYPE html>
<html lang='es'>
	<head>
		<meta charset='utf-8'/>
		<title>Hospedaje El Edén</title>
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/non-responsive.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/datepicker.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap-timepicker.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
	</head>
	<body>
		<section class='container'>
			<div class='row'>
				<section class='col-xs-12'>
					<div class='cuadro'>
						<h2 class='text-center'> Gestión de Habitaciones</h2>
						<div class='row'>
							<div class='col-xs-2'>
								<h3>Tipos
									<button class='btn btn-primary btn-sm pull-right' data-toggle='modal' data-target='#mdlGestionTipo'>
										<i class='fa fa-plus'></i> Nuevo
									</button>
								</h3>
								<div id='lstTiposHabitacion' class='list-group list-habitaciones'>
								</div>
								<input type='hidden' id='idTipoHabitacion' value='' descripcion=''>
							</div>
							<div class='col-xs-5'>
								<div class='row'>
									<div class='col-xs-6'>
										<h3>Habitaciones
											<button class='btn btn-primary btn-sm' id='btnAgregarHabitacion'><i class='fa fa-plus'></i> Nuevo</button>
										</h3>
									</div>
									<div class='col-xs-6 chk-habitacion-estado'>
										<div class='form-horizontal'>
											<div class='form-group'>
												<div class="checkbox col-xs-6">
													<label>
														<input type="checkbox" id='chbDisponible' checked>
														Disponible
													</label>
												</div>
												<div class="checkbox col-xs-5">
													<label>
														<input type="checkbox" id='chbOcupado' checked>
														Ocupado
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class='row'>
									<div class='col-xs-12'>
										<div id='lstHabitaciones' class='list-group list-habitaciones'>
										</div>
										<input type='hidden' id='idHabitacion' value='' descripcion=''>
									</div>
								</div>
							</div>
							<div class='col-xs-5'>
								<h3>Información
									<button class='btn btn-primary btn-sm pull-right' id='btnActualizarCuenta'><i class='fa fa-refresh'></i> Actualizar</button>
								</h3>
								<div class='cuadro' id='infoCuenta'>
									<div class='row'>
										<div class="col-xs-12">
											<div class='form-horizontal'>
												<div class='form-group'>
													<label class='col-xs-4 control-label'>Cliente</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='clienteHabitacion'></p>
													</div>
												</div>
												<div class='form-group'>
													<label class='col-xs-4 control-label'>Entrada</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='entradaHabitacion'></p>
													</div>
												</div>
												<div class='form-group'>
													<label class='col-xs-4 control-label'>Estancia</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='estanciaHabitacion'></p>
													</div>
												</div>
												<div class='form-group'>
													<label class='col-xs-4 control-label'># Personas</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='npersonasHabitacion'></p>
													</div>
												</div>
												<div class='form-group'>
													<label class='col-xs-4 control-label'>Motivo</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='motivoHabitacion'></p>
													</div>
												</div>
												<div class='form-group'>
													<label class='col-xs-4 control-label'>Observación</label>
													<div class='col-xs-8'>
														<p class='form-control-static' id='observacionHabitacion'></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>

		<!-- MODALES -->
		<div class='modal fade' id='mdlActualizarCuenta' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
						<h4 class='modal-title' id='myModalLabel'>Actualizar Cuenta</h4>
					</div>
					<div class='modal-body'>
						<?php $this->load->view('frmActualizarCuenta'); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="mdlGestionTipo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<form class='form-horizontal' id='frmGestionTipo'>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Agregar Tipo</h4>
						</div>
						<div class="modal-body">
							<div class='form-group'>
								<label for='txtDescripcion' class='col-xs-3 control-label'><code>*</code>Tipo</label>
								<div class='col-xs-7'>
									<input type='text' id='txtDescripcion' class='form-control input-sm' placeholder='Tipo' required />
								</div>
							</div>
							<div class='form-group'>
								<label for='txtPrecio' class='col-xs-3 control-label'><code>*</code>Precio</label>
								<div class='col-xs-5'>
									<input type='text' id='txtPrecio' class='form-control input-sm' placeholder='S/.30.00' required />
								</div>
							</div>
							<input type='hidden' id='idTipoEditar' idTipoHabitacion='-1'/>
						</div>
						<div class="modal-footer">
							<button type="button" id='btnEliminarTipo' class="btn-sm btn btn-danger pull-left hide">Eliminar</button>
							<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-sm btn-primary">Agregar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="mdlGestionHabitacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form class='form-horizontal' id='frmGestionHabitacion'>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Agregar Habitacion</h4>
						</div>
						<div class="modal-body">
							<div class='form-group'>
								<label for='txtDescripcionHabitacion' class='col-xs-3 control-label'><code>*</code>Descripcion</label>
								<div class='col-xs-7'>
									<input type='text' id='txtDescripcionHabitacion' class='form-control input-sm' placeholder='Descripción' required />
								</div>
							</div>
							<div class='form-group'>
								<label for='cboTipoHabitacion' class='col-xs-3 control-label'><code>*</code>Tipo</label>
								<div class='col-xs-5'>
									<div class='dropdown'>
										<button id='dropdownMenu2' class='btn btn-default drodown-toggle' data-toggle='dropdown' type='button'>
											Seleccionar
											<span class='caret'></span>
										</button>
										<ul id='cboServicio' class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu2'>
										</ul>
									</div>
								</div>
								<input type='hidden' id='mdlIdTipo' value='-1'/>
							</div>
							<div class='form-group'>
								<label for='txtPrecioHabitacion' class='col-xs-3 control-label'><code>*</code>Precio</label>
								<div class='col-xs-2'>
									<input type='text' id='txtPrecioHabitacion' class='form-control input-sm' placeholder='S/. 30.00' required />
								</div>
							</div>
							<input type='hidden' id='idHabitacionEditar' idHabitacion='-1'/>
						</div>
						<div class="modal-footer">
							<button type="button" id='btnEliminarHabitacion' class="btn-sm btn btn-danger pull-left hide">Eliminar</button>
							<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-sm btn-primary">Agregar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-datepicker.js'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-timepicker.js'></script>
		<script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
		<script>
			var base_url = '<?php echo base_url(); ?>';
			$(document).ready(function(e){
				$('#mnu-habitaciones').addClass('active');
				timepicker($('#factxthoraegreso'));
				datepicker($('#factxtfechaegreso'));
				$('#facchbegreso').change(function(e){
					var flag = $(this).is(':checked');
					$('#factxtfechaegreso').prop('disabled', !flag);
					$('#factxthoraegreso').prop('disabled', !flag);
				});
				$('#facbtncancelar').on('click', function(e){
					e.preventDefault();
					$('#mdlActualizarCuenta').modal('hide');
				});
				$('#frmActualizarCuenta').on('submit', function(e){
					e.preventDefault();
					//('#facchbegreso').is(':checked');
					$.ajax({
						url: base_url + 'operaciones/actualizar_detalle',
						type: 'post',
						dataType: 'json',
						data: {
							idCuenta : $('#facidcuenta').val(),
							idOperacion : $('#facidoperacion').val(),
							idDetalleOperacion : $('#faciddetalleoperacion').val(),
							cantidad : $('#factxtcantidad').val(),
							precio : $('#factxtmonto').val()
						},
						success: function(data){
						}
					});

				});

				$('#frmGestionTipo').on('submit', function(e){
					e.preventDefault();
					var id = document.getElementById('idTipoEditar').getAttribute('idTipoHabitacion');
					var descripcion = $('#txtDescripcion').val();
					var precio = $('#txtPrecio').val();

					var url = ( id == '-1') ? 'habitaciones/agregar_tipo_habitacion' : 'habitaciones/actualizar_tipo_habitacion';

					$.ajax({
						url: '<?php echo base_url();?>' + url,
						dataType: 'json',
						data:{
							idTipo : id,
							descripcion : descripcion,
							precio : precio
						},
						type: 'post',
						success: function(result){
							cargarTiposHabitacion();
							$('#mdlGestionTipo').modal('hide');
						}
					});
				});

				$('#frmGestionHabitacion').on('submit', function(e){
					e.preventDefault();
					var id = document.getElementById('idHabitacionEditar').getAttribute('idHabitacion');
					var descripcion = $('#txtDescripcionHabitacion').val();
					var tipoHab = $('#mdlIdTipo').val();
					var precio = $('#txtPrecioHabitacion').val();
					var url = ( id == '-1') ? 'habitaciones/agregar_habitacion' : 'habitaciones/actualizar_habitacion';

					$.ajax({
						url: '<?php echo base_url();?>' + url,
						dataType: 'json',
						type: 'post',
						data:{
							idHabitacion: id,
							descripcion : descripcion,
							idTipoHabitacion: tipoHab,
							precio : precio
						},
						success: function(result){
							cargarHabitaciones(0);
							$('#mdlGestionHabitacion').modal('hide');
						}
					});

				});

				$('#btnEliminarTipo').on('click', function(e){
					e.preventDefault();
					$.ajax({
						url: '<?php echo base_url(); ?>habitaciones/eliminar_tipo_habitacion',
						dataType: 'json',
						data:{
							idTipo : document.getElementById('idTipoEditar').getAttribute('idTipoHabitacion')
						},
						type: 'post',
						success: function(result){
							cargarTiposHabitacion();
							$('#mdlGestionTipo').modal('hide');
						}
					});
				});

				cargarTiposHabitacion();
				cargarHabitaciones(0);

				$('#lstTiposHabitacion').on('click', 'a.list-group-item', function(e){
					e.preventDefault();
					cargarHabitaciones(this.getAttribute('idTipoHabitacion'));
				}).on('mouseenter', 'a.list-group-item', function(e){
					var button = this.children[0];
					$(button).removeClass('hide');
				}).on('mouseleave', 'a.list-group-item', function(e){
					var button = this.children[0];
					$(button).addClass('hide');
				});

				$('#lstHabitaciones').on('click', 'a.list-group-item', function(e){
					e.preventDefault();
					cargar_cuenta(this.getAttribute('idHabitacion'));
					$('#idHabitacion').val(this.getAttribute('idHabitacion'));
				}).on('mouseenter', 'a.list-group-item', function(e){
					var button = this.children[1];
					$(button).removeClass('hide');
				}).on('mouseleave', 'a.list-group-item', function(e){
					var button = this.children[1];
					$(button).addClass('hide');
				});

				$('#lstTiposHabitacion').on('click', 'span.gestionItemVenta', function(e){
					e.preventDefault();
					e.stopPropagation();
					var a = this.parentNode;

					var idTipo = a.getAttribute('idTipoHabitacion');
					var descripcion = a.getAttribute('descripcion');
					var precio = a.getAttribute('precio');


					$('#txtDescripcion').val(descripcion);
					$('#txtPrecio').val(precio);
					document.getElementById('idTipoEditar').setAttribute('idTipoHabitacion', idTipo);

					var button = document.getElementById('btnEliminarTipo');
					$(button).removeClass('hide');

					$('#mdlGestionTipo').modal('show');
				});

				$('#lstHabitaciones').on('click', 'span.gestion', function(e){
					e.preventDefault();
					e.stopPropagation();

					var a = this.parentNode;

					var idHabitacion = a.getAttribute('idHabitacion');
					var descripcion = a.getAttribute('descripcion');
					var idTipoHabitacion = a.getAttribute('idTipoHabitacion');
					var precio = a.getAttribute('precio');

					$('#txtDescripcionHabitacion').val(descripcion);

					var tipos = document.querySelectorAll('a[cboidTipoHabitacion]');

					var flag = true;
					var i = 0;
					while(flag && i < tipos.length){
						if(tipos[i].getAttribute('cboidtipoHabitacion') == idTipoHabitacion){
							$(tipos[i]).trigger('click');
							flag = false;
						}
						i++;
					}

					var button = document.getElementById('btnEliminarHabitacion');
					$(button).removeClass('hide');

					$('#txtPrecioHabitacion').val(precio);
					document.getElementById('idHabitacionEditar').setAttribute('idHabitacion', idHabitacion);

					$('#mdlGestionHabitacion').modal('show');
				});

				$('#mdlGestionTipo').on('shown.bs.modal', function(){
					$('#txtDescripcion').focus();
				});

				$('#mdlGestionHabitacion').on('shown.bs.modal', function(){
					$('#txtDescripcionHabitacion').focus();
				});

				$('#mdlGestionTipo').on('hidden.bs.modal', function(){
					limpiar_mdlGestionTipo();
				});

				$('#mdlGestionHabitacion').on('hidden.bs.modal', function(){
					limpiar_mdlGestionHabitacion();
				});

				$('#btnAgregarHabitacion').on('click', function(e){
					$('#mdlGestionHabitacion').modal('show');
				});

				$('#facbtncalcdias').on('click', function(e){
					var inicio = $('#facfechaingreso').val();
					var fin = $('#factxtfechaegreso').val();
					var dias = calcular_dias(inicio, fin);
					$('#factxtcantidad').val(dias).change();
				});

				$('#btnActualizarCuenta').on('click', function(e){
					e.preventDefault();
					$.ajax({
						url: base_url + 'operaciones/get_detalle_operacion',
						type: 'post',
						dataType: 'json',
						data: {
							idHabitacion: $('#idHabitacion').val()
						},
						success: function(result){
							if(result['state']){
								var data = result['data']['0'];
								$('#faciddetalleoperacion').val(data['idDetalleOperacion']);
								$('#facidoperacion').val(data['idOperacion']);
								$('#facidcuenta').val(data['idCuenta']);
								$('#facfechaingreso').val(data['fechaEntrada']);
								$('#factxtcantidad').val(data['cantidad']);
								$('#factxtmonto').val(data['precio']);
								$('#facprecioservicio').val(data['monto']);
							}else{

							}
						}
					});
					$('#mdlActualizarCuenta').modal('show');
				});

				$('#factxtcantidad').on('change', function(e){
					var cantidad = $(this).val();
					var monto = $('#facprecioservicio').val();
					$('#factxtmonto').val(cantidad * monto);
				});

				$("#cboServicio").on('click', 'a', function(e){
					e.preventDefault();
					$('#mdlIdTipo').val(this.getAttribute('cboidTipoHabitacion'));
					$('#dropdownMenu2').html($(this).text() + " <span class='caret'></span>");
				});

			});

			function cargar_cuenta(idHabitacion){
				$.ajax({
					url: '<?php echo base_url(); ?>cuentas/info_cuenta',
					dataType: 'json',
					type: 'post',
					data:{
						idHabitacion: idHabitacion
					},
					success: function(result) {
						var result = result['data'];
						if(result.length > 0){
							var cliente = (result[0].persona == '' || result[0].persona == null)? result[0].empresa : result[0].persona;
							$('#clienteHabitacion').text(cliente);
							cargar_detalle_cuenta(result[0].idCuenta);
						}else{
							limpiar_cuenta();
						}
					}
				});
			}

			function cargar_detalle_cuenta(idCuenta){
				$.ajax({
					url: '<?php echo base_url(); ?>cuentas/detalle_cuenta',
					dataType: 'json',
					type: 'post',
					data: {
						idCuenta: idCuenta
					},
					success: function(result) {
						var result = result['data'];
						$('#entradaHabitacion').text(result[0].fechaEntrada + '  a las  ' + result[0].horaEntrada);
						var mod = result[0].modalidad == 'D' ? 'días' : 'horas';
						$('#estanciaHabitacion').text(result[0].cantidad + ' ' + mod);
						$('#npersonasHabitacion').text(result[0].numPersonas);
						$('#motivoHabitacion').text(result[0].motivo);
						$('#observacionHabitacion').text(result[0].observacion);
					}
				});
			}

			function limpiar_cuenta(){
				$('#clienteHabitacion').text('');
				$('#entradaHabitacion').text('');
				$('#estanciaHabitacion').text('');
				$('#npersonasHabitacion').text('');
				$('#motivoHabitacion').text('');
				$('#observacionHabitacion').text('');
			}

			function limpiar_mdlGestionTipo(){
				$('#txtDescripcion').val('');
				$('#txtPrecio').val('');
				document.getElementById('idTipoEditar').setAttribute('idTipoHabitacion', '-1');

				var button = document.getElementById('btnEliminarTipo');
				$(button).addClass('hide');
			}

			function limpiar_mdlGestionHabitacion(){
				$('#txtDescripcionHabitacion').val('');
				var tipos = document.querySelectorAll('a[cboidTipoHabitacion]');
				$(tipos[0]).trigger('click');
				$('#txtPrecioHabitacion').val('');
				document.getElementById('idHabitacionEditar').setAttribute('idHabitacion', '-1');

				var button = document.getElementById('btnEliminarHabitacion');
				$(button).addClass('hide');
			}

			function cargarHabitaciones(idTipo){
				var estado = 0;
				estado = document.getElementById('chbDisponible').checked ? estado | 1 : estado;
				estado = document.getElementById('chbOcupado').checked ? estado | 2 | 4 : estado;

				$.ajax({
					url : "<?php echo base_url().'habitaciones/obtener_habitaciones_tipo'; ?>",
					dataType : 'json',
					data:{
						idTipo : idTipo,
						estado : estado
					},
					type : 'POST',
					success: function(result){
						var result = result['data'];
						var x = "";
						for(i = 0; i < result.length; i++){
							x += "<a href='#' class='list-group-item' idHabitacion='";
							x += result[i].idServicio + "' ";
							x += "descripcion='" + result[i].descripcion + "' precio='" + result[i].precio + "' ";
							x += "idTipoHabitacion='" + result[i].idTipoHabitacion + "'>";
							x += "<i class='fa fa-circle ";
							switch(result[i].estado){
								case 'A':
									x += 'verde';
									break;
								 case 'O':
									x += 'rojo';
									break;
								 case 'P':
									x += 'azul';
									break;
							}
							x += " estado'></i> ";
							x += result[i].descripcion;
							x += "<span class='hide pull-right fa fa-cog gestion'></span>";
							x += "</a>";
						}
						$('#lstHabitaciones').html(x);
					}
				});
			}

			function cargarTiposHabitacion(){
				$.ajax({
					url : "<?php echo base_url().'habitaciones/obtener_tipos_habitacion'; ?>",
					dataType : 'json',
					type : 'POST',
					success: function(result){
						var result = result['data'];
						var x = "<a href='#' class='list-group-item' idTipoHabitacion='0'>Todos</a>";
						var cbo = "";//"<li class='itmCboServicios' role='presentation'><a href='#' role='menuitem' tabindex='-1' cboidTipoHabitacion='0'>Todos</a></li>";
						for(i = 0; i < result.length; i++){
							x += "<a href='#' class='list-group-item item-tipo' idTipoHabitacion='";
							x += result[i].idTipo + "' precio='" + result[i].precio;
							x += "' descripcion='" + result[i].descripcion + "'>";
							x += result[i].descripcion;
							x += "<span class='hide pull-right fa fa-cog gestionItemVenta'></span>" + "</a>";
							cbo += "<li class='itmCboServicios' role='presentation'><a href='#' role='menuitem' tabindex='-1' cboidTipoHabitacion='";
							cbo += result[i].idTipo + "'>" + result[i].descripcion + "</a></li>";
						}
						$('#lstTiposHabitacion').html(x);
						$('#cboServicio').html(cbo);
					}
				});
			}

		</script>
	</body>
</html>
