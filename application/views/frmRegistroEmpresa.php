<form class='form-horizontal' role='form' id='frmRegistroEmpresa'>
    <div class='form-group'>
        <label for='txtRUC-empresa' class='col-xs-4 control-label'><code>*</code>RUC</label>
        <div class='col-xs-3'>
            <input id='txtRUC-empresa' type='text' class='form-control input-sm' placeholder='00000000000' required/>
        </div>
    </div>
    <div class='form-group'>
        <label for='txtRazonSocial' class='col-xs-4 control-label'><code>*</code>Razón social</label>
        <div class='col-xs-6'>
            <input type='text' id='txtRazonSocial' class='form-control input-sm' placeholder='Razón Social' required />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtProcedencia-empresa' class='col-xs-4 control-label'><code>*</code>Procedencia</label>
        <div class='col-xs-6'>
            <input id='txtProcedencia-empresa' type='search' class='form-control' autocomplete='off' placeholder='Distrito' required />
            <input id='idProcedenciaEmpresa' type='hidden' value='-1'/>
        </div>
    </div>
    <div class='form-group'>
        <label for='txtDireccion' class='col-xs-4 control-label'>Dirección</label>
        <div class='col-xs-6'>
            <input type='text' id='txtDireccion' class='form-control input-sm' placeholder='Dirección' />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtTelefono-empresa' class='col-xs-4 control-label'>Teléfono</label>
        <div class='col-xs-3'>
            <input type='tel' id='txtTelefono-empresa' class='form-control input-sm' placeholder='000-000000' />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtEmail-empresa' class='col-xs-4 control-label'>Correo</label>
        <div class='col-xs-5'>
            <input type='email' id='txtCorreo-empresa' class='form-control input-sm' placeholder='xxxxxx@xxxxxx.xxx' />
        </div>
    </div>
    <div id='alertRegistroEmpresa'>
    </div>
    <div class='form-group'>
        <div class='col-xs-3 col-xs-offset-4'>
            <input type='button' data-dismiss='modal' value='Cancelar' class='btn btn-danger btn-sm col-xs-12' />
        </div>
        <div class='col-xs-3'>
            <input id='btnRegistrarEmpresa' type='submit' value='Registrar' class='btn btn-primary btn-sm col-xs-12' />
        </div>
    </div>
</form>
