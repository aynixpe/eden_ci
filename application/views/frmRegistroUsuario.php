<form class='form-horizontal' role='form' id='frmRegistroUsuario'>
    <input type='hidden' id='fru-idUsuario' value='-1' />
    <div class='form-group'>
        <label for='cboTipoUsuario' class='col-xs-4 control-label'>Tipo Usuario</label>
        <div class='col-xs-3'>
            <select id='cboTipoUsuario' class='form-control input-sm'>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='txtNombre' class='col-xs-4 control-label'>Nombre</label>
        <div class='col-xs-6'>
            <input id='txtNombre' type='text' class='form-control input-sm' placeholder='Juan Perez' required />
        </div>
    </div>
    <div class='form-group'>
        <label for='txtUsuario' class='col-xs-4 control-label'>Usuario</label>
        <div class='col-xs-6'>
            <input id='txtUsuario' type='text' class='form-control input-sm' placeholder='jperez' />
        </div>
    </div>
    <div class='form-group' id='fg-pass'>
        <label for='txtPass' class='col-xs-4 control-label'>Contraseña</label>
        <div class='col-xs-6'>
            <input type='password' id='txtPass' class='form-control input-sm' placeholder='******' required />
        </div>
    </div>

    <div id='alertRegistroUsuario'>
    </div>
    <div class='form-group'>
        <div class='col-xs-3 col-xs-offset-4'>
            <input type='button' data-dismiss='modal' value='Cancelar' class='btn btn-danger btn-sm col-xs-12' />
        </div>
        <div class='col-xs-3'>
            <input id='btnRegistrarUsuario' type='submit' value='Registrar' class='btn btn-primary btn-sm col-xs-12' />
        </div>
    </div>
</form>
