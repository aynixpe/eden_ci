<form class='form-inline' id='frmRegistroComponente'>
    <div class='row'>
        <label class='control-label col-xs-2'>Comprobante</label>
        <div class='col-xs-2'>
            <select id='cboTipoComprobante' class='form-control input-sm' style='width:100%;'>
            </select>
        </div>
        <div class='col-xs-2'>
            <select id='cboSerie' class='form-control input-sm' style='width:100%;'>
            </select>
        </div>
        <div class='col-xs-3'>
            <input type='text' class='form-control input-sm' disabled>
        </div>
        <div class='col-xs-3'>
            <div class="input-group input-group-sm">
                <input id="txtHora" class="form-control input-sm" type="text">
                <span class="input-group-btn">
                    <button class='btn btn-sm btn-default' type='button'><i class="fa fa-clock-o"></i></button>
                </span>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-xs-12'>
            <h5>Ítems</h5>
            <table class='table table-hover'>
                <thead>
                    <tr>
                        <th>Ítem</th>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Monto</th>
                    </tr>
                </thead>
                <tbody id='tblItemsComprobante'>
                </tbody>
            </table>
        </div>
    </div>
    <div class='row'>
        <div class='col-xs-2 col-xs-offset-10'>
            <button type='submit' class='btn btn-primary btn-sm col-xs-12'>Pagar</button>
        </div>
    </div>
</form>
