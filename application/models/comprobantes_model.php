<?php
class Comprobantes_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function get_tipos(){
		$result = $this->db->get('tipocomprobante');
		return $result->result_array();
	}

	/*
	SERIE: select * from serie where idTipoComprobante = 1 AND estado LIKE 'A'
	*/
	public function get_series($idTipoComprobante){
		$this->db->select('*');
		$this->db->from('serie');
		$this->db->where('idTipoComprobante', $idTipoComprobante);
		$this->db->where('estado', 'A');
		$result = $this->db->get();

		return $result->result_array();
	}

	/*
	ULTIMO COMPROBANTE DE LA SERIE: select MAX(numero) from comprobante where idSerie = 1
	*/
	public function insert($idTipoComprobante, $idCliente, $fecha, $monto, $idUsuario, $detalles){
		$this->db->select('idSerie');
		$this->db->from('serie');
		$this->db->where('idTipoComprobante', $idTipoComprobante);
		$this->db->where('estado', 'A');
		$result = $this->db->get()->result_array();
		$idSerie = $result[0]['idSerie'];

		$this->db->select_max('numero');
		$this->db->from('comprobante');
		$this->db->where('idSerie', $idSerie);
		$result = $this->db->get()->result_array();
		$num_comprobante = $result[0]['numero'];

		$comprobante = array(
			'numero' => $num_comprobante + 1,
			'idCliente' => $idCliente,
			'fecha' => $fecha,
			'monto' => $monto,
			'idSerie' => $idSerie,
			'idUsuario' => $idUsuario,
			'estado' => 'G'
		);

		$detalles = json_decode($detalles);

		$x = new stdClass();
		if($detalles != $x){
			$this->db->trans_begin();
			$this->db->insert('comprobante', $operacion);

			$this->db->select_max('idComprobante');

			foreach ($detalles as $detalle) {
				$data = array(
					'idComprobante' => $idComprobante,
					'idServicio' => $detalle->idServicio,
					'cantidad' => $detalle->cantidad,
					'precio' => $detalle->cantidad,
					'subtotal' => $detalle->subtotal,
					'estado' => 'G'
				);
				$this->insert('detallecomprobante', $data);
				$habitacion = array(
					'estado' => 'P'
					);
				$this->db->where('idServicio', $detalle->idServicio);
				$this->db->update('servicio', $habitacion);
			}
			$this->db->trans_complete();
			return $this->db->trans_status();
		}
		return false;
	}
}
?>
