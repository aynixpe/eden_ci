<?php
class Cuentas_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

    function getCuenta($idHabitacion){
        $this->db->select("c.idCuenta");
        $this->db->select("cl.razonSocial_empresa as empresa");
        $this->db->select("CONCAT(cl.apellidos_cliente, ', ', cl.nombres_cliente) as persona", FALSE);
        $this->db->from('cuenta as c');
        $this->db->join('cliente as cl', 'cl.idCliente = c.idCliente');
        $this->db->where('idServicio', $idHabitacion);
        $this->db->where('c.estado', 'G');
        $result = $this->db->get();

        return $result->result_array();
    }

    function getCuentas_query($query){
        $this->db->select('c.idCuenta, cl.tipo, cl.nombres_cliente, cl.apellidos_cliente, cl.razonSocial_empresa');
        $this->db->select('s.descripcion');
        $this->db->select('c.monto');
        $this->db->from('cuenta as c');
        $this->db->join('cliente as cl', 'cl.idCliente = c.idCliente');
        $this->db->join('servicio as s', 's.idServicio = c.idServicio');
        $this->db->where('c.estado','G');
        $this->db->like('cl.documento', $query);
        $this->db->or_like('cl.RUC', $query);
        $this->db->or_like('cl.nombres_cliente', $query);
        $this->db->or_like('apellidos_cliente', $query);
        $this->db->or_like('razonSocial_empresa', $query);
        $this->db->or_like('s.descripcion', $query);

        $result = $this->db->get();

        return $result->result_array();
    }

    function getCuenta_byCliente($idCliente){
        $this->db->select("c.idCuenta, s.descripcion, c.monto");
        $this->db->from("cuenta as c");
        $this->db->join("servicio as s", "c.idServicio = s.idServicio");
        $this->db->where("c.idCliente", $idCliente);
        $this->db->where("c.estado", 'G');
        $result = $this->db->get();

        return $result->result_array();
    }

    function getDetalleCuenta($idCuenta){
        $this->db->select("idDetalleOperacion, modalidad, cantidad, numPersonas, precio, motivo, fechaEntrada, horaEntrada, observacion");
        $this->db->from('detalleoperacion');
        $this->db->where('idCuenta', $idCuenta);
        $this->db->where('estado', 'G');

        $result = $this->db->get();

        return $result->result_array();
    }

    function getDetallesCuenta($idCuenta){
        $this->db->select("d.idDetalleOperacion, s.descripcion, d.cantidad, d.precio");
        $this->db->from("detalleoperacion as d");
        $this->db->join("servicio as s", 's.idServicio = d.idServicio');
        $this->db->where("d.estado = 'G' and d.idCuenta LIKE '".$idCuenta."'");

        $result = $this->db->get();

        return $result->result_array();
    }

	function getCuentas_byCliente($idCliente){
		$this->db->select('c.idCuenta, s.descripcion, d.cantidad, d.precio');
		$this->db->from('cuenta as c');
		$this->db->join('detalleoperacion as d', 'c.idCuenta = d.idCuenta');
		$this->db->join('servicio as s', 's.idServicio = d.idServicio');
		$this->db->like('c.estado', 'G');
		$this->db->like('d.estado', 'G');
		$this->db->where('c.idCliente', $idCliente);

		$result = $this->db->get();
		return $result->result_array();
	}

	function recalcular($idCuenta){
		$this->db->select('SUM(precio) AS total');
		$this->db->from('detalleoperacion');
		$this->db->where('idCuenta', $idCuenta);
		$result = $this->db->get()->result_array();

		$total = $result[0]['total'];

		$data = array(
			"monto" => $total
		);

		$this->db->where('idCuenta', $idCuenta);
		$this->db->trans_begin();
		$this->db->update('cuenta', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

}
?>
