<?php
class Usuarios_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function login($usuario, $password){
        $this->db->where('usuario', $usuario);
        $this->db->where('pass', $password);
        $query = $this->db->get('usuario');
        if($query->num_rows() == 1){
            return $query->row();
        }
        return false;
    }

    public function get_usuario($idUsuario){
        $this->db->select('u.idUsuario, u.nombre, u.usuario, t.descripcion, u.idTipoUsuario');
        $this->db->from('usuario as u');
        $this->db->join('tipousuario as t', 't.idTipoUsuario = u.idTipoUsuario');
        $this->db->where('idUsuario', $idUsuario);
        $result = $this->db->get();

        return $result->result_array()[0];
    }

    public function get_usuarios(){
        $this->db->select('u.idUsuario, u.nombre, u.usuario, t.descripcion as tipo');
        $this->db->from('usuario as u');
        $this->db->join('tipousuario as t', 't.idTipoUsuario = u.idTipoUSuario');
        $result = $this->db->get();

        return $result->result_array();
    }

    public function get_tipos(){
        $this->db->select('*');
        $this->db->from('tipousuario');
        $result = $this->db->get();

        return $result->result_array();
    }

    public function insert_tipo($descripcion){
        $data = array(
            'descripcion' => $descripcion,
            'p_servicio' => 0,
            'p_operacion' => 0,
            'p_cliente' => 0,
            'p_usuario' => 0,
            'p_cuenta' => 0,
            'p_comprobante' => 0
        );

        $this->db->trans_begin();
        $this->db->insert('tipousuario', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function get_tipo($idTipo){
        $this->db->select("idtipousuario, descripcion, p_servicio as '0', p_operacion as '1', p_cliente as '2', p_usuario as '3', p_cuenta as '4', p_comprobante as '5'");
        $this->db->from('tipousuario');
        $this->db->where('idtipousuario', $idTipo);
        $result = $this->db->get();

        return $result->result_array();
    }

    public function update_tipo($idTipo, $descripcion){
        $data = array(
            'descripcion' => $descripcion
        );
        $this->db->where('idTipoUsuario', $idTipo);
        $this->db->update('tipousuario', $data);
        return true;
    }

    public function update_permisos($idtipo, $permisos){
        $data = array(
            'p_servicio' => $permisos[0],
            'p_operacion' => $permisos[1],
            'p_cliente' => $permisos[2],
            'p_usuario' => $permisos[3],
            'p_cuenta' => $permisos[4],
            'p_comprobante' => $permisos[5]
        );
        $this->db->where('idTipoUsuario', $idtipo);
        $this->db->update('tipousuario', $data);
        return true;
    }

    public function delete_tipo($idtipo){
        $this->db->where('idtipousuario', $idtipo);
        $this->db->delete('tipousuario');
        return true;
    }

    public function insert($nombre, $usuario, $password, $idTipo){
        $data = array(
            "nombre" => $nombre,
            "usuario" => $usuario,
            "pass" => $password,
            "idTipoUsuario" => $idTipo,
            "estado" => 'A'
        );
        $this->db->trans_begin();
        $this->db->insert('usuario', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function update($idUsuario, $nombre, $usuario, $idTipo){
        $data = array(
            "nombre" => $nombre,
            "usuario" => $usuario,
            "idTipoUsuario" => $idTipo
        );
        $this->db->where('idUsuario', $idUsuario);
        $this->db->trans_begin();
        $this->db->update('usuario', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function comprobar_permiso($idUsuario, $accion, $valor){
        $data = array();
        $tabla = '';
        switch($accion){
            case SERVICIOS:
                $tabla = 'p_servicio';
                break;
            case OPERACIONES:
                $tabla = 'p_operacion';
                break;
            case CLIENTES:
                $tabla = 'p_cliente';
                break;
            case USUARIOS:
                $tabla = 'p_usuario';
                break;
            case CUENTAS:
                $tabla = 'p_cuenta';
                break;
            case COMPROBANTES:
                $tabla = 'p_comprobante';
                break;
        }

        $this->db->select($tabla.' as permiso');
        $this->db->from('tipousuario as t');
        $this->db->join('usuario as u', 't.idTipoUsuario = u.idTipoUsuario');
        $this->db->where('idUsuario', $idUsuario);
        $result = $this->db->get();
        $permiso = $result->result_array()[0]['permiso'];
        $data['state'] = ( ($permiso & $valor) == $valor );
        if( ! $data['state'])
            $data['error'] = unserialize(ERR_PERMISO);
        return $data;
    }
}
?>
