<?php
class Habitaciones_model extends CI_Model{
    private $disponible = 1;
    private $ocupado_pagado = 2;
    private $ocupado_no_pagado = 4;
    private $inactivo = 8;

	function __construct(){
        parent::__construct();
		$this->load->database();
	}

	function getTiposHabitacion(){
        $this->db->where('estado LIKE "A"');
		$query = $this->db->get('tipohabitacion');

		return $query->result_array();
	}

    function addTipoHabitacion($descripcion, $precio){
        $data = array(
            'descripcion' => $descripcion,
            'precio' => $precio,
            'estado' => 'A'
        );
        $this->db->trans_begin();
        $this->db->insert('tipohabitacion', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    function addHabitacion(){
        $data = array(
            'descripcion' => $this->input->post('descripcion'),
            'tipo' => 'H',
            'idTipoHabitacion' => $this->input->post('idTipoHabitacion'),
            'idUsuario' => 1,
            'precio' => $this->input->post('precio'),
            'estado' => 'A'
        );

        $this->db->trans_begin();
        $this->db->insert('servicio', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    function updateTipoHabitacion(){
        $data = array(
            'descripcion' => $this->input->post('descripcion'),
            'precio' => $this->input->post('precio')
        );

        $this->db->where('idTipo', $this->input->post('idTipo'));
        $this->db->trans_begin();
        $this->db->update('tipohabitacion', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    function updateHabitacion(){
        $data = array(
            'descripcion' => $this->input->post('descripcion'),
            'idTipoHabitacion' => $this->input->post('idTipoHabitacion'),
            'precio' => $this->input->post('precio')
        );

        $this->db->where('idServicio', $this->input->post('idHabitacion'));
        $this->db->trans_begin();
        $this->db->update('servicio', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    function getLastTipoHabitacion(){
        $this->db->select('idTipo, descripcion, precio');
        $this->db->from('tipoHabitacion');
        $this->db->order_by('idTipo', 'desc');
        $this->db->limit(1);

        $result = $this->db->get();

        return $result->result_array();
    }

	function getHabitaciones($estado){
        $this->db->select('s.idServicio, s.descripcion, s.precio, th.precio, s.estado');
		$this->db->from('servicio AS s');
		$this->db->join('tipohabitacion AS th', 'th.idTipo = s.idTipoHabitacion');
        $where = "(s.estado LIKE 'x' ";
        $this->db->order_by('s.descripcion');

        if( ($this->disponible & $estado) == $this->disponible){
            $where .= " OR s.estado LIKE 'A' ";
        }
        if( ($this->ocupado_pagado & $estado) == $this->ocupado_pagado){
            $where .= " OR s.estado LIKE 'O' ";
        }
        if( ($this->ocupado_no_pagado & $estado) == $this->ocupado_no_pagado){
            $where .= " OR s.estado LIKE 'P' ";
        }
        if( ($this->inactivo & $estado) == $this->inactivo){
            $where .= " OR s.estado LIKE 'I' ";
        }

        $where .= ")";
        $this->db->where($where);

		$result = $this->db->get();

		return $result->result_array();
	}

	function getHabitaciones_tipo($idTipo, $estado){
		$this->db->select('s.idServicio, s.descripcion, s.idTipoHabitacion, s.precio AS precio, th.precio AS tipoPrecio, s.estado');
		$this->db->from('servicio AS s');
		$this->db->join('tipohabitacion AS th', 'th.idTipo = s.idTipoHabitacion');
		$where = "(s.estado LIKE 'x' ";
        $this->db->order_by('s.descripcion');

        if( ($this->disponible & $estado) == $this->disponible){
            $where .= " OR s.estado LIKE 'A' ";
        }
        if( ($this->ocupado_pagado & $estado) == $this->ocupado_pagado){
            $where .= " OR s.estado LIKE 'O' ";
        }
        if( ($this->ocupado_no_pagado & $estado) == $this->ocupado_no_pagado){
            $where .= " OR s.estado LIKE 'P' ";
        }
        if( ($this->inactivo & $estado) == $this->inactivo){
            $where .= " OR s.estado LIKE 'I' ";
        }

        $where .= ")";
        $this->db->where($where);

		if($idTipo > 0)
			$this->db->where('s.idTipoHabitacion', $idTipo);
		$result = $this->db->get();

		return $result->result_array();
	}

     function eliminarTipoHabitacion(){
        $data = array(
            'estado' => 'I'
        );

        $this->db->where('idTipo', $this->input->post('idTipo'));
        return $this->db->update('tipohabitacion', $data);
    }

}
?>
