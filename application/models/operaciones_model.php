<?php
class Operaciones_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function insertarOperacion($idCliente, $monto, $idUsuario, $observacion, $fecha, $hora, $detalles){
		$this->db->select_max('idOperacion');
		$idOperacion1 = $this->db->get('operacion')->result_array();

        $operacion = array(
            'idCliente' => $idCliente,
            'tipo' => 'V',
            'monto' => $monto,
            'idUsuario' => $idUsuario,
            'estado' => 'G',//G=Generado; A=Anulado
            'observacion' => $observacion,
            'fecha' => $fecha,
            'hora' => $hora
        );


        $detalles = json_decode($detalles);

        $x = new stdClass();
        if($detalles != $x){
            $this->db->trans_begin();
            $this->db->insert('operacion', $operacion);

            $this->db->select_max('idOperacion');
            $idOperacion2 = $this->db->get('operacion')->result_array();

            $idOperacion = ($idOperacion1 == $idOperacion2) ? -1 : $idOperacion2[0]['idOperacion'];
            foreach ($detalles as $detalle) {
                $cuenta = array(
                    'idCliente' => $idCliente,
                    'idServicio' => $detalle->idHabitacion,
                    'monto' => $monto,
                    'idUsuario' => $idUsuario,
                    'estado' => 'G'
                );

                $idCuenta1 = $this->db->get('cuenta')->result_array();
                $this->db->insert('cuenta', $cuenta);

                $this->db->select_max('idCuenta');
                $idCuenta2 = $this->db->get('cuenta')->result_array();
                $idCuenta = ($idCuenta1 == $idCuenta2) ? -1 : $idCuenta2[0]['idCuenta'];

                $data = array(
                    'idOperacion' => $idOperacion,
                    'idCliente' => $detalle->idHuesped,
                    'idServicio' => $detalle->idHabitacion,
                    'numPersonas' => $detalle->numPersonas,
                    'modalidad' => $detalle->modalidad,
                    'cantidad' => $detalle->cantidad,
                    'precio' => $detalle->precio,
                    'motivo' => $detalle->motivo,
                    'fechaEntrada' => $detalle->fechaEntrada,
                    'horaEntrada' => $detalle->horaEntrada,
                    'observacion' => $detalle->observacion,
                    'idCuenta' => $idCuenta,
                    'estado' => 'G' //G:Generado; A:Anulado
                );
                $this->db->insert('detalleoperacion', $data);
                $habitacion = array(
                    'estado' => 'O'
                );

                $this->db->where('idServicio', $detalle->idHabitacion);
                $this->db->update('servicio', $habitacion);
            }
            $this->db->trans_complete();
            return $this->db->trans_status();
        }
        return false;
	}

	public function get_detalle_operacion($idHabitacion, $estado){
		$this->db->select('do.*, th.precio as monto');
		$this->db->from('detalleoperacion as do');
		$this->db->join('servicio as s', 's.idServicio = do.idServicio');
		$this->db->join('tipohabitacion as th', 'th.idTipo = s.idTipoHabitacion');
		$this->db->where('do.idServicio', $idHabitacion);
		$this->db->where('do.estado', $estado);
		$result = $this->db->get();

		return $result->result_array();
	}

	public function actualizar_detalle($idDetalle, $cantidad, $monto){
		$data = array(
			"cantidad" => $cantidad,
			"precio" => $monto
		);

		$this->db->where('idDetalleOperacion', $idDetalle);

		$this->db->trans_begin();
		$this->db->update('detalleoperacion', $data);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}
}
?>
