<?php
class Clientes_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

    function getUbigeo($query){
        $this->db->select("idUbigeo AS id");
        $this->db->select("CONCAT(distrito, ' - ', provincia, ' - ', departamento) AS name", false);
		$this->db->from('ubigeo');
        $this->db->like('distrito', $query);
        $result = $this->db->get();
		
		return $result->result_array();
    }

	function getClientes_activos(){
		$this->db->where('estado', 'A');
		return $this->db->get('cliente');
	}

	function buscarClientes_activos($query){
		$this->db->select('idCliente, tipo, nombres_cliente, razonSocial_empresa, documento, ruc');
		$where = "estado = 'A' AND nombres_cliente like ";
		$where .= "'%$query%' OR apellidos_cliente like ";
		$where .= "'%$query%' OR documento like '%$query%' ";
		$where .= "OR ruc like '%$query%' OR razonSocial_empresa ";
		$where .= "like '%$query%' LIMIT 10;";

		$this->db->where($where);
		$result = $this->db->get('cliente');

		return $result->result_array();
	}

    function buscarEmpresas_activas($query){
		$this->db->select('idCliente, tipo, razonSocial_empresa, ruc, telefono, correo');
		$where = "estado = 'A' AND tipo LIKE 'E' AND (razonSocial_empresa like ";
		$where .= "'%$query%' ";
		$where .= "OR ruc like '%$query%' ) ;";
		$this->db->where($where);
		$result = $this->db->get('cliente');

		return $result->result_array();
	}

	function buscarPersonas_activas($query){
		$this->db->select("c.idCliente, c.tipo, c.nombres_cliente, c.apellidos_cliente, c.documento, c.ruc, c.telefono, c.correo");
        $this->db->from("cliente as c");
		$where = "c.tipo LIKE 'P' AND (c.nombres_cliente like '%";
		$where .= "$query%' OR c.apellidos_cliente like ";
		$where .= "'%$query%' OR c.documento like '%$query%' ";
		$where .= "OR c.ruc like '%$query%' ) LIMIT 10;";

        $this->db->where($where);
		$result = $this->db->get();
		return $result->result_array();
	}

	function buscarPersonas($query, $inactivo, $noHospedado){
        $estado = ($inactivo == 'true') ? "" : "c.estado LIKE 'A' AND ";
        $noHospedado = ($noHospedado == 'true') ?"" : "do.idDetalleOperacion is not null AND ";

        $this->db->distinct();
		$this->db->select("c.idCliente, c.tipo, c.nombres_cliente, c.apellidos_cliente, c.documento, c.ruc, c.telefono, c.correo");
        $this->db->from("cliente as c");
        $this->db->join("detalleoperacion as do", "c.idCliente = do.idCliente", 'left');
		$where = $estado.$noHospedado."c.tipo LIKE 'P' AND (c.nombres_cliente like '%";
		$where .= "$query%' OR c.apellidos_cliente like ";
		$where .= "'%$query%' OR c.documento like '%$query%' ";
		$where .= "OR c.ruc like '%$query%' ) LIMIT 10;";

        $this->db->where($where);
		$result = $this->db->get();
		return $result->result_array();
	}

	function insertarCliente($tipo, $tipoDocumento,
			$documento, $ruc, $nombres, $apellidos, $ubigeo,
			$telefono, $correo, $idUsuario, $razonSocial,
			$direccion){

        if($tipo == 'P'){
            $data = array(
                'tipo' => $tipo,
                'tipoDocumento' => $tipoDocumento,
                'documento' => $documento,
                'ruc' => $ruc,
                'nombres_cliente' => $nombres,
                'apellidos_cliente' => $apellidos,
                'idUbigeo' => $ubigeo,
                'telefono' => $telefono,
                'correo' => $correo,
                'idUsuario' => $idUsuario,
                'estado' => 'A'
            );
        }else if($tipo == 'E'){
            $data = array(
                'tipo' => $tipo,
                'ruc' => $ruc,
                'idUbigeo' => $ubigeo,
                'razonSocial_empresa' => $razon_social,
                'direccion_empresa' => $direccion,
                'telefono' => $telefono,
                'correo' => $correo,
                'idUsuario' => $idUsuario,
                'estado' => 'A'
            );
        }

        $this->db->trans_begin();
		$this->db->insert('cliente', $data);
        $this->db->trans_complete();

        return $this->db->trans_status();
	}
}
?>
